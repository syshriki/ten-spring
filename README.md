# README #

Ten is a social networking app built currently as a POC for a client, designed for iPhone and Android phones. Ten is similar to Instagram, it lets you share moments via messages and images with your friends and family. You must register an account but after that you can follow other users to see their shared moments on your newsfeed. The difference between this app and Instagram, you moments are only visible for 10 minutes before they disappear! But! For every like you get on your moment a minute is added to its lifetime.

### What is this repository for? ###

* This repo contains the backend application code for Ten.
* This project is still on-going

### Requirements ###
* JDK 8+
* MYSQL 5.6+

### Setup ###
*  Setup SQL schema
*  hMailServer