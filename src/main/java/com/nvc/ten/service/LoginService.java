package com.nvc.ten.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.LoginDao;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.utility.ObjectTransformUtility;

@Service
public class LoginService {
	@Autowired
	LoginDao loginDao;
	
	@Autowired
	ObjectTransformUtility objectTransformUtility;
	
	public UserExtended login(int idUser){
		return objectTransformUtility.userToToUserExtended(loginDao.selectUser(idUser));
	}
	
}
