package com.nvc.ten.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.NotificationDao;
import com.nvc.ten.exception.ExceedsMaxNotificationCount;
import com.nvc.ten.exception.InvalidUserId;
import com.nvc.ten.exception.UnauthorizedNotificationRead;
import com.nvc.ten.models.json.Notification;
import com.nvc.ten.models.presistent.NotificationTo;
import com.nvc.ten.utility.ObjectTransformUtility;

@Service
public class NotificationService {
	@Autowired
	NotificationDao notificationDao;

	@Autowired
	ObjectTransformUtility objectTransformUtility;

	@Value("${notification.count.max}")
	private int NOTIFICATION_MAX_COUNT;
	
	private boolean returnImages;

	// TODO make sure negative counts and offsets are handled
	public List<Notification> getNotifications(int offset, int count,int idCurrentUser) {
		if (count > NOTIFICATION_MAX_COUNT || count < 0)
			throw new ExceedsMaxNotificationCount();

		List<Notification> notifications = objectTransformUtility.notificationTosToNotifications(notificationDao.selectUnreadNotificationByIdUser(idCurrentUser, offset,count),idCurrentUser);
		//Issue #24
//		notifications.forEach((notification)->
//			notificationDao.updateUnreadNotificationRead(notification.getIdNotification())
//		);
		
		return notifications;

	}
	
	public int readNotification(int idNotification,int idCurrentUser){
		NotificationTo notificationTo = notificationDao.selectNotificationByIdNotification(idNotification);
		if(notificationTo.getIdAffected()==idCurrentUser)
			return notificationDao.updateUnreadNotificationRead(idNotification);
		else 
			throw new UnauthorizedNotificationRead();
	}

	public int getNotificationCount(int idCurrentUser) {
		return notificationDao.countUnreadNotificationByIdUser(idCurrentUser);
	}

	public boolean isReturnImages() {
		return returnImages;
	}

	public void setReturnImages(boolean returnImages) {
		this.returnImages = returnImages;
	}
}
