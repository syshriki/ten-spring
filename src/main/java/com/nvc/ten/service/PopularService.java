package com.nvc.ten.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.PopularDao;
import com.nvc.ten.exception.ExceedsMaxTimedGlobalCount;
import com.nvc.ten.exception.ExceedsMaxTimedLocalCount;
import com.nvc.ten.exception.ExceedsMaxTimerLocalCount;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.User;
import com.nvc.ten.utility.ObjectTransformUtility;

@Service
public class PopularService {

	@Autowired
	PopularDao popularDao;

	@Autowired
	ObjectTransformUtility objectTransformUtility;
	
	@Value("${popular.timed.local.count.max}")
	private int TIMED_LOCAL_COUNT_MAX;
	
	@Value("${popular.timer.local.count.max}")
	private int TIMER_LOCAL_COUNT_MAX;
	
	@Value("${popular.timed.global.count.max}")
	private int TIMED_GLOBAL_COUNT_MAX;
	
	@Value("${popular.timer.global.count.max}")
	private int TIMER_GLOBAL_COUNT_MAX;
	
	public List<Post> getToptimedLocal(int offset, int count,int idCurrentUser){
		if(offset>TIMED_LOCAL_COUNT_MAX)
			throw new ExceedsMaxTimedLocalCount();
		return objectTransformUtility.postTosToPosts(popularDao.selectTopTimedLocalByIdPost(idCurrentUser, offset, count),idCurrentUser);
	}
	
	public List<Post> getToptimedGlobal(int offset, int count,int idCurrentUser){
		if(offset>TIMED_GLOBAL_COUNT_MAX)
			throw new ExceedsMaxTimedGlobalCount();
		return objectTransformUtility.postTosToPosts(popularDao.selectTopTimedGlobalByIdPost(idCurrentUser, offset, count), idCurrentUser);
	}
	
	public List<User> getToptimersLocal(int offset, int count,int idCurrentUser){
		if(offset>TIMER_LOCAL_COUNT_MAX)
			throw new ExceedsMaxTimerLocalCount();
		return objectTransformUtility.userTosToUsers(popularDao.selectTopTimerLocalByIdPost(idCurrentUser, offset, count));
	}

	public List<User> getToptimersGlobal(int offset, int count,int idCurrentUser){
		if(offset>TIMER_GLOBAL_COUNT_MAX)
			throw new ExceedsMaxTimedGlobalCount();
		return objectTransformUtility.userTosToUsers(popularDao.selectTopTimerGlobalByIdPost(idCurrentUser, offset, count));
	}
}
