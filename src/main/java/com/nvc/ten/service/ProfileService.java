package com.nvc.ten.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nvc.ten.dao.ProfileDao;
import com.nvc.ten.exception.AlreadyExistsEmailAddress;
import com.nvc.ten.exception.ExceedsMaxFollowersCount;
import com.nvc.ten.exception.ExceedsMaxFollowersSearchCount;
import com.nvc.ten.exception.ExceedsMaxFollowingCount;
import com.nvc.ten.exception.ExceedsMaxFollowingSearchCount;
import com.nvc.ten.exception.ImageNotFound;
import com.nvc.ten.exception.InvalidPostId;
import com.nvc.ten.exception.InvalidUserId;
import com.nvc.ten.exception.InvalidUsername;
import com.nvc.ten.exception.MustWaitUntilActivePostsExpire;
import com.nvc.ten.exception.UnauthorizedImageRetrieval;
import com.nvc.ten.exception.UnauthorizedProfileRetrieval;
import com.nvc.ten.exception.UnauthorizedProfileUpdate;
import com.nvc.ten.exception.AlreadyExistsUser;
import com.nvc.ten.exception.AlreadyExistsUsername;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.Profile;
import com.nvc.ten.models.json.User;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;
import com.nvc.ten.utility.ImageUtility;
import com.nvc.ten.utility.ObjectTransformUtility;
import com.nvc.ten.utility.ServiceUtility;

@Service
public class ProfileService {

	@Autowired
	ProfileDao profileDao;

	@Autowired
	ObjectTransformUtility objectTransoformUtility;

	@Autowired
	ServiceUtility serviceUtility;

	@Autowired
	ImageUtility imageUtility;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Value("${profile.followers.count.max}")
	private int FOLLOWER_COUNT_MAX;

	@Value("${profile.following.count.max}")
	private int FOLLOWING_COUNT_MAX;

	@Value("${profile.followers.search.count.max}")
	private int FOLLOWER_SEARCH_COUNT_MAX;

	@Value("${profile.following.search.count.max}")
	private int FOLLOWING_SEARCH_COUNT_MAX;

	@Value("${profile.posts.active.count.max}")
	private int POSTS_ACTIVE_COUNT_MAX;

	@Value("${profile.posts.active.count.default}")
	private int POSTS_ACTIVE_COUNT_DEFAULT;

	@Value("${profile.posts.all.count.max}")
	private int POSTS_ALL_COUNT_MAX;


	public Profile getProfile(String username,int idCurrentUser) {
		if (username == null)
			throw new InvalidUsername();

		UserTo userTo = serviceUtility.getUserToByUsername(username);

		if (userTo == null)
			throw new InvalidUsername();

		Profile profile = new Profile();

		if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
			throw new UnauthorizedProfileRetrieval();

		if (serviceUtility.isFollowing(idCurrentUser, userTo.getIdUser()))
			profile.setActivePosts(this.getActivePosts(username, 0,
					POSTS_ACTIVE_COUNT_DEFAULT,idCurrentUser));

		profile.setUser(objectTransoformUtility.userToToUserExtended(userTo,
				idCurrentUser));
		profile.setBestTimedPost(objectTransoformUtility.postToToPost(
				profileDao.selectPostMostLikedByIdUser(userTo.getIdUser()),
				idCurrentUser));
		profile.setCommentCount(profileDao.countCommentsOnIdUser(userTo
				.getIdUser()));
		profile.setFollowersCount(profileDao.countFollowersByIdUser(userTo
				.getIdUser()));
		profile.setFollowingCount(profileDao.countFollowingByIdUser(userTo
				.getIdUser()));
		profile.setAllPostCount(profileDao.countPostsAllByIdUser(userTo
				.getIdUser()));
		return profile;
	}

	public Post getBestTimedPost(String username, int idCurrentUser) {

			if (username == null)
				throw new InvalidUsername();

			UserTo userTo = serviceUtility.getUserToByUsername(username);

			if (userTo == null)
				throw new InvalidUsername();

			if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();

		return objectTransoformUtility
				.postToToPost(
						profileDao.selectPostMostLikedByUsername(username),
						idCurrentUser);
	}

	public UserExtended getUser(int idUser, int idCurrentUser) {
			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();

			return objectTransoformUtility.userToToUserExtended(
				serviceUtility.getUserToByIdUser(idUser), idCurrentUser);
	}

	public List<UserExtended> getFollowing(int idUser, int offset, int count, int idCurrentUser) {
			if (count > FOLLOWER_COUNT_MAX)
				throw new ExceedsMaxFollowingCount();

			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
			
		return objectTransoformUtility.userTosToUserExtended(
				profileDao.selectFollowingByIdUser(idUser, offset, count),
				idCurrentUser);
	}

	public List<UserExtended> findFollowing(int idUser, String username,
			int offset, int count,int idCurrentUser) {

		if (count > FOLLOWING_SEARCH_COUNT_MAX)
			throw new ExceedsMaxFollowingSearchCount();

		if (idUser <= 0)
			throw new InvalidUserId();

		if (username == null)
			throw new InvalidUsername();

		UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

		if (userTo == null)
			throw new InvalidUserId();

		if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
			throw new UnauthorizedProfileRetrieval();

		return objectTransoformUtility.userTosToUserExtended(profileDao
				.selectFollowingLikeUsername(idUser, username, offset, count),
				idCurrentUser);
	}

	public List<UserExtended> findFollowers(int idUser, String username,
			int offset, int count,int idCurrentUser) {
		if (count > FOLLOWER_SEARCH_COUNT_MAX)
			throw new ExceedsMaxFollowersSearchCount();

		if (idUser <= 0)
			throw new InvalidUserId();

		if (username == null)
			throw new InvalidUsername();

		UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

		if (userTo == null)
			throw new InvalidUserId();

		if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
			throw new UnauthorizedProfileRetrieval();

		return objectTransoformUtility.userTosToUserExtended(profileDao
				.selectFollowersLikeUsername(idUser, username, offset, count),
				idCurrentUser);
	}

	public List<UserExtended> getFollowers(int idUser, int offset, int count, int idCurrentUser) {


			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
			
		return objectTransoformUtility.userTosToUserExtended(
				profileDao.selectFollowersByIdUser(idUser, offset, count),
				idCurrentUser);
	}

	public List<User> getTopLikers(String username,int idCurrentUser) {

		if (username == null)
			throw new InvalidUsername();

		UserTo userTo = serviceUtility.getUserToByUsername(username);

		if (userTo == null)
			throw new InvalidUsername();

		if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
			throw new UnauthorizedProfileRetrieval();
		
		return objectTransoformUtility.userTosToUsers(profileDao
				.selectLikersTopByIdUser(userTo.getIdUser()));
	}

	public List<Post> getAllPosts(String username, int offset, int count, int idCurrentUser) {
			if (username == null)
				throw new InvalidUsername();

			UserTo userTo = serviceUtility.getUserToByUsername(username);

			if (userTo == null)
				throw new InvalidUsername();

			if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
			
		return objectTransoformUtility.postTosToPosts(
				profileDao.selectPostAllByUsername(username, offset, count),
				idCurrentUser);
	}

	public List<Post> getActivePosts(String username, int offset, int count, int idCurrentUser) {
			if (username == null)
				throw new InvalidUsername();

			UserTo userTo = serviceUtility.getUserToByUsername(username);

			if (userTo == null)
				throw new InvalidUsername();

			if (serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser())
					&& !serviceUtility.isFollowing(idCurrentUser,
							userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();

			return objectTransoformUtility.postTosToPosts(
				profileDao.selectPostActiveByUsername(username, offset, count),
				idCurrentUser);
	}

	public int updateProfile(UserExtended user,int idCurrentUser) {
		UserTo userTo = serviceUtility.getUserToByIdUser(idCurrentUser);
		
		//Issue #28
		if(user.getIsPrivate()!=userTo.isPrivate()){
			if(!this.getActivePosts(userTo.getUsername(), 0, 1,idCurrentUser).isEmpty())
				throw new MustWaitUntilActivePostsExpire();
		}
			
		if (!userTo.getUsername().equals(user.getUsername())) {
			if (profileDao.countByUsername(user.getUsername()) > 0)
				throw new AlreadyExistsUsername();
		}
		
		//Email is not required when updating profile
		if (!userTo.getEmail().equals(user.getEmail())) {
			if (profileDao.countByEmail(user.getEmail()) > 0 && user.getEmail()!=null)
				throw new AlreadyExistsEmailAddress();
			else
				user.setEmail(userTo.getEmail());
		}else{
			user.setEmail(userTo.getEmail());
		}

		return profileDao.updateUserProfile(userTo.getUsername(),
				user.getDescription(), user.getEmail(), user.getWebsite(),
				user.getIsPrivate() ? 1 : 0, idCurrentUser);
	}
	
	public int updatePassword(UserExtended user,int idCurrentUser) {

		// Reset password
		UserTo userTo = serviceUtility.getUserToByIdUser(idCurrentUser);

		if (user.getPassword() != null)
			user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		return profileDao.updateUserPassword(user.getPassword(),userTo.getIdUser());
	}

	public int updateProfileImage(MultipartFile imageFile,int idCurrentUser) {

		BufferedImage bufferedImage = null;
		UserTo userTo = serviceUtility.getUserToByIdUser(idCurrentUser);
		InputStream in;
		try {
			in = new ByteArrayInputStream(imageFile.getBytes());
			bufferedImage = ImageIO.read(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String imageSrc = "pro"
				+ "-"
				+ System.currentTimeMillis()
				+ "."
				+ imageUtility.SUPPORTED_IMAGE_FORMATS.get(imageFile
						.getContentType());
		imageUtility.saveImageFileToDisk(bufferedImage, imageSrc, idCurrentUser);
		return profileDao.updateUserImage(imageSrc, idCurrentUser);
	}

	public int countAllPosts(int idUser) {
			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
		return profileDao.countPostsAllByIdUser(idUser);
	}

	public int countActivePosts(int idUser) {
			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
		return profileDao.countPostsActiveByIdUser(idUser);
	}

	public int countFollwing(int idUser) {
			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
		return profileDao.countFollowingByIdUser(idUser);
	}

	public int countFollwers(int idUser ) {
			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
		return profileDao.countFollowersByIdUser(idUser);
	}

	public int countLikes(int idUser) {
			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
		return profileDao.countLikesOnIdUser(idUser);
	}

	public int countComments(int idUser) {
			if (idUser <= 0)
				throw new InvalidUserId();

			UserTo userTo = serviceUtility.getUserToByIdUser(idUser);

			if (userTo == null)
				throw new InvalidUserId();

			if (serviceUtility.isBlocked(idUser, userTo.getIdUser()))
				throw new UnauthorizedProfileRetrieval();
		return profileDao.countCommentsOnIdUser(idUser);
	}

	public String getImageFormat(String username,int idCurrentUser) {

		if (username == null)
			throw new InvalidUsername();

		UserTo userTo = serviceUtility.getUserToByUsername(username);

		if (userTo == null)
			throw new InvalidUsername();

		if (!serviceUtility.isFollowing(idCurrentUser, userTo.getIdUser())
				&& userTo.isPrivate()
				|| serviceUtility.isBlocked(idCurrentUser, userTo.getIdUser()))
			throw new UnauthorizedImageRetrieval();

		String imageSrc = userTo.getImageSrc();

		if (imageSrc == null || imageSrc.equals(""))
			throw new ImageNotFound();

		String extension = imageSrc.substring(imageSrc.lastIndexOf('.') + 1,
				imageSrc.length());
		return imageUtility.getImageType(extension);
	}

	public byte[] getImageBytes(String username) {
		UserTo userTo = serviceUtility.getUserToByUsername(username);
		String imageSrc = userTo.getImageSrc();
		return imageSrc != null ? imageUtility.loadImage(userTo.getIdUser() + "/"
				+ imageSrc) : null;
	}
}
