package com.nvc.ten.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.NotificationDao;
import com.nvc.ten.dao.ProfileDao;
import com.nvc.ten.dao.UserActionDao;
import com.nvc.ten.dao.UtilityDao;
import com.nvc.ten.exception.AlreadyFollowing;
import com.nvc.ten.exception.AlreadyPending;
import com.nvc.ten.exception.AlreadyUsedResetToken;
import com.nvc.ten.exception.ExpiredResetToken;
import com.nvc.ten.exception.InvalidCredential;
import com.nvc.ten.exception.InvalidEmailAddress;
import com.nvc.ten.exception.InvalidFollow;
import com.nvc.ten.exception.InvalidResetToken;
import com.nvc.ten.exception.InvalidUserId;
import com.nvc.ten.exception.NotFollowing;
import com.nvc.ten.exception.NotPending;
import com.nvc.ten.exception.UnauthorizedFollow;
import com.nvc.ten.exception.UserAlreadyBlocked;
import com.nvc.ten.exception.UserBlocked;
import com.nvc.ten.exception.UserNotBlocked;
import com.nvc.ten.models.json.Notification;
import com.nvc.ten.models.json.User;
import com.nvc.ten.models.presistent.FollowTo;
import com.nvc.ten.models.presistent.PasswordResetToken;
import com.nvc.ten.models.presistent.UserTo;
import com.nvc.ten.push.INotifier;
import com.nvc.ten.utility.ObjectTransformUtility;
import com.nvc.ten.utility.ServiceUtility;
import com.nvc.ten.validator.PasswordValidator;

@Service
public class UserActionService {
	@Autowired
	Environment env;
	
	@Autowired
	UserActionDao userActionDao;
	
	@Autowired
	NotificationDao notificationDao;
	
	@Autowired
	UtilityDao utilityDao;
	
	@Autowired
	ProfileDao profileDao;

	@Autowired
	ServiceUtility serviceUtility;
	
	@Autowired
	ObjectTransformUtility objectTransformUtility;
	
	@Autowired
	INotifier notifierIosImpl;
	
    @Autowired
    private JavaMailSender javaMailSender;
    
	@Autowired
	PasswordEncoder passwordEncoder;
    
	@Value("${useraction.pending.count.max}")
	private int PENDING_COUNT_MAX;
	
	@Value("${useraction.resetpassword.message}")
	private String RESET_MESSAGE;
	
	@Value("${email.from}")
	private String EMAIL_FROM;
	
    public boolean emailResetToken(String email) {
    	UserTo userTo = serviceUtility.getUserToByEmail(email);
	    if (userTo == null) {
	        throw new InvalidEmailAddress();
	    }
	 
	    String token = UUID.randomUUID().toString();
	    this.createPasswordResetTokenForUser(userTo, token);
	 
	    SimpleMailMessage mail = 
	      constructResetTokenEmail(token, userTo);
	    try{
	    	javaMailSender.send(mail);
	    	return true;
	    }catch(MailException e){
	    	return false;
	    }
    }
    
    public User resetPassword(String token, String email, String password){
	    if(!password.matches(PasswordValidator.PASSWORD_VALIDATION_REGEX))
	    	throw new InvalidCredential("Invalid password");
	    UserTo userTo = utilityDao.selectUserByEmail(email);
	    PasswordResetToken passToken = this.getPasswordResetToken(token);
	    
	    if (passToken == null || userTo.getIdUser() != passToken.getIdUser()) {
	       throw new InvalidResetToken();
	    }
	    
	    if(passToken.getIsUsed())
	    	throw new AlreadyUsedResetToken();
	 
	    Calendar cal = Calendar.getInstance();
	    if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
	    	throw new ExpiredResetToken();
	    }
	    
		userActionDao.updatePasswordresetByToken(token);
		profileDao.updateUserPassword(passwordEncoder.encode(password),userTo.getIdUser());
		
		return  objectTransformUtility.userToToUser(userTo);
    }
    
    public void createPasswordResetTokenForUser(final UserTo user, final String token) {
        final PasswordResetToken myToken = new PasswordResetToken(token, user.getIdUser());
        Date oldDate = new Date();
        //Tokens last for 12 hrs
        userActionDao.insertPasswordResetToken(token, user.getIdUser(), new Date(oldDate.getTime() + TimeUnit.HOURS.toMillis(12)));
    }
    
    public PasswordResetToken getPasswordResetToken(String token){
    	return userActionDao.selectPasswordResetTokenByToken(token);
    }
    
    private SimpleMailMessage constructResetTokenEmail(
    		   String token, UserTo userTo) {
    		    SimpleMailMessage email = new SimpleMailMessage();
    		    email.setTo(userTo.getEmail());
    		    email.setFrom(EMAIL_FROM);
    		    email.setSubject("Ten Password Reset");
    		    email.setText(RESET_MESSAGE + " " + token);
    		    return email;
    }

	public int blockUser(int idBlocked,int idCurrentUser) {
		if(idBlocked<=0)
			throw new InvalidUserId();
		
		if(userActionDao.selectBlockByIdBlockedAndIdBlocker(idBlocked, idCurrentUser)!=null)
			throw new UserAlreadyBlocked();
		
		//Unfollow user being blocked
		if(serviceUtility.isFollowing(idBlocked, idCurrentUser))
			userActionDao.deleteFollow(idCurrentUser, idBlocked);
		
		//Issue #79
		if(serviceUtility.isFollowing(idCurrentUser,idBlocked))
			userActionDao.deleteFollow(idBlocked,idCurrentUser);
		
		return userActionDao.insertBlock(idBlocked, idCurrentUser);
	}

	public int unblockUser(int idBlocked,int idCurrentUser) {
		if(idBlocked<=0)
			throw new InvalidUserId();
		
		if(userActionDao.selectBlockByIdBlockedAndIdBlocker(idBlocked, idCurrentUser)==null)
			throw new UserNotBlocked();
		
		return userActionDao.deleteBlock(idBlocked, idCurrentUser);
	}

	public int followUser(int idRecipient,int idCurrentUser) {
		if(idRecipient <= 0)
			throw new InvalidUserId();
		
		//Issue #74 user cannot follow self
		if(idRecipient==idCurrentUser)
			throw new InvalidFollow();
		
		FollowTo followTo = serviceUtility.getFollowTo(idRecipient, idCurrentUser);
		//Following where an existing follow already exists between users
		if(followTo!=null){
			if(followTo.getApproved()==0)
				throw new AlreadyPending();
			
			if(followTo.getApproved()==1 )
				throw new AlreadyFollowing();
		}
		
		if(userActionDao.selectBlockByIdBlockedAndIdBlocker(idCurrentUser, idRecipient)!=null)
			throw new UnauthorizedFollow();
		
		if(userActionDao.selectBlockByIdBlockedAndIdBlocker(idRecipient, idCurrentUser)!=null)
			throw new UserBlocked();
		
		UserTo userTo = serviceUtility.getUserToByIdUser(idRecipient);
		
		if(userTo==null)
			throw new InvalidUserId();
		
		//If a user gets rejected they can try to follow again
		if(followTo!=null && followTo.getApproved()==-1){
			if(!userTo.isPrivate()){
				int status = userActionDao.updateFollowerApprove(idRecipient,idCurrentUser);
				//Now following
				notifyFollowUser(idRecipient,idCurrentUser);//Issue #58
				return status;
			}else{
				//User is still private
				int status = userActionDao.updateFollowerPending(idRecipient,idCurrentUser);
				notifyFollowPrivateUser(idRecipient,idCurrentUser);//Issue #58
				return status;
			}
		}
		
		if(userTo.isPrivate()){
			int status = userActionDao.insertFollowPending(idCurrentUser, idRecipient);
			//Success inserting pending request
			if(status>0)
				notifyFollowPrivateUser(idCurrentUser, userTo.getIdUser());//Issue #58
			return status;	
		}else{
			int status = userActionDao.insertFollowApproved(idCurrentUser, idRecipient);
			//Success inserting follow approval
			if(status>0)
				notifyFollowUser(idCurrentUser, idRecipient);//Issue #58
			return status;
		}
	}

	//Issue #58
	private void notifyFollowUser(int idAffectingUser, int idAffectedUser){
		if(notificationDao.insertNotification(Notification.NotificationType.FOLLOWING.toString(),idAffectingUser,idAffectedUser)>0)
			notifierIosImpl.followingNotification(idAffectingUser,idAffectedUser);
		if(notificationDao.insertNotification(Notification.NotificationType.FOLLOW.toString(),idAffectedUser,idAffectingUser)>0)
			notifierIosImpl.followNotification(idAffectedUser,idAffectingUser);
	}
	
	//Issue #58
	private void notifyFollowPrivateUser(int idAffectingUser, int idAffectedUser){
		if(notificationDao.insertNotification(Notification.NotificationType.FRIENDREQUEST.toString(), idAffectingUser, idAffectedUser)>0)
			notifierIosImpl.friendRequestNotification(idAffectingUser, idAffectedUser);
	}
	
	public int unfollowUser(int idRecipient,int idCurrentUser) {
		if(idRecipient <= 0)
			throw new InvalidUserId();
		
		if(serviceUtility.getFollowTo(idRecipient, idCurrentUser)==null)
			throw new NotFollowing();
		
		return userActionDao.deleteFollow(idCurrentUser, idRecipient);
	}

	public int approveFollowRequest(int idRequestee,int idCurrentUser) {
		if(idRequestee <= 0)
			throw new InvalidUserId();
		
		if(serviceUtility.isPending(idCurrentUser, idRequestee)){
			int status = userActionDao.updateFollowerApprove(idRequestee,idCurrentUser);
			//follow is sucessfully updated
			if(status>0){
				//Push notification Issue #58
				if(notificationDao.insertNotification(Notification.NotificationType.FOLLOW.toString(),idCurrentUser,idRequestee)>0)
					notifierIosImpl.followNotification(idRequestee,idCurrentUser);
				//Changed on 1/12/2017 notification appears reversed
				if(notificationDao.insertNotification(Notification.NotificationType.FOLLOWING.toString(),idCurrentUser,idRequestee)>0)
					notifierIosImpl.followingNotification(idCurrentUser, idRequestee);
			}
			return status;
		}
		
		throw new NotPending();
	}

	public int rejectFollowRequest(int idRequestee,int idCurrentUser) {
		if(idRequestee <= 0)
			throw new InvalidUserId();
		
		if(serviceUtility.isPending(idCurrentUser,idRequestee))
			return userActionDao.updateFollowerReject(idRequestee,idCurrentUser);
		
		throw new NotPending();
	}
	
	public List<User> getPendingRequests(int offset, int count,int idCurrentUser) {
		return objectTransformUtility.userTosToUsers(userActionDao.selectFollowerPending(idCurrentUser, offset,count));
	}

	
	public int cancelPendingRequests(int idUser) {
		return userActionDao.deleteFollowPending(idUser, idUser);
	}
}
