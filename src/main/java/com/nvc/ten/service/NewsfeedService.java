package com.nvc.ten.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.NewsfeedDao;
import com.nvc.ten.exception.ExceedsMaxNewsfeedCount;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.utility.ObjectTransformUtility;
import com.nvc.ten.utility.ServiceUtility;

@Service
public class NewsfeedService {
	
	@Autowired
	NewsfeedDao newsfeedDao;
	
	@Autowired
	ServiceUtility serviceUtility;
	
	@Autowired
	ObjectTransformUtility objectTransformUtility;
	
	@Value("${newsfeed.count.max}")
	private int NEWSFEED_COUNT_MAX;

	private boolean returnImages;
	
	//TODO newsfeed grabbing is very inefficient
	public List<Post> getNewsfeed(int offset,int count,int idCurrentUser) {
		if(count>NEWSFEED_COUNT_MAX)
			throw new ExceedsMaxNewsfeedCount();
		List<PostTo> postTos = newsfeedDao.selectNewsfeedPostsByIdUser(idCurrentUser, offset, count);
		return objectTransformUtility.postTosToPosts(postTos, idCurrentUser);
	}

	public boolean isReturnImages() {
		return returnImages;
	}

	public void setReturnImages(boolean returnImages) {
		this.returnImages = returnImages;
	}
}
