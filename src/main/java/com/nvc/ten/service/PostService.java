package com.nvc.ten.service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.NotificationDao;
import com.nvc.ten.dao.PostDao;
import com.nvc.ten.dao.presistent.NotificationMysqlDao;
import com.nvc.ten.dao.presistent.PostMysqlDao;
import com.nvc.ten.exception.AlreadyLiked;
import com.nvc.ten.exception.AlreadyReposted;
import com.nvc.ten.exception.BadImageFormat;
import com.nvc.ten.exception.EmptyPost;
import com.nvc.ten.exception.ExceedsMaxCommentCount;
import com.nvc.ten.exception.ExceedsMaxLikersCount;
import com.nvc.ten.exception.ExpiredPost;
import com.nvc.ten.exception.ImageNotFound;
import com.nvc.ten.exception.InvalidComment;
import com.nvc.ten.exception.InvalidCommentId;
import com.nvc.ten.exception.InvalidPostId;
import com.nvc.ten.exception.NotFoundPost;
import com.nvc.ten.exception.RepostOwnPost;
import com.nvc.ten.exception.UnauthorizedComment;
import com.nvc.ten.exception.UnauthorizedCommentRemoval;
import com.nvc.ten.exception.UnauthorizedCommentRetrieval;
import com.nvc.ten.exception.UnauthorizedImageRetrieval;
import com.nvc.ten.exception.UnauthorizedLike;
import com.nvc.ten.exception.UnauthorizedLikeRetrieval;
import com.nvc.ten.exception.UnauthorizedPostRemoval;
import com.nvc.ten.exception.UnauthorizedPostRetrieval;
import com.nvc.ten.exception.UnauthorizedRepost;
import com.nvc.ten.models.json.Comment;
import com.nvc.ten.models.json.Notification;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;
import com.nvc.ten.push.INotifier;
import com.nvc.ten.utility.ImageUtility;
import com.nvc.ten.utility.ObjectTransformUtility;
import com.nvc.ten.utility.ServiceUtility;

@Service
public class PostService {
	@Autowired
	PostDao postDao;
	
	@Autowired
	NotificationDao notificationDao;
	
	@Autowired
	ServiceUtility serviceUtility;

	@Autowired
	ObjectTransformUtility objectTransformUtility;

	@Autowired
	ImageUtility imageUtility;
	
	@Autowired
	INotifier notifierIosImpl;
	
	@Value("${post.comment.count.max}")
	private int COMMENT_COUNT_MAX;

	@Value("${post.liker.count.max}")
	private int LIKER_COUNT_MAX;

	public Post createPost(Post post,int idCurrentUser) {
		if (post == null || post.getText() == null
				&& post.getImageFile() == null)
			throw new EmptyPost();

		Date datestamp = new Date();
		post.setDatestamp(datestamp);
		Date expiration = new Date(System.currentTimeMillis() + 10 * 60 * 1000);
		post.setExpiration(expiration);
		int idPost = postDao.insertPost(
				post.getText() == null ? "" : post.getText(), idCurrentUser, post
						.getUserReposter() != null ? post.getUserReposter()
						.getIdUser() : 0, post.getImageFile() == null ? 0 : 1,
				expiration, datestamp);

		post.setIdPost(idPost);

		Set<String> hastags = findHashtagsInText(post.getText());
		Set<String> usernameMentions = findMentionsInText(post.getText());

		// Insert hashtags
		hastags.forEach((hashtag) -> {
			int idHashtags = createHashtag(hashtag, idCurrentUser, idPost);
			postDao.insertPostHashTag(idPost, idHashtags);
		});

		// Insert mentions notifications
		for(String username: usernameMentions){
			UserTo user = serviceUtility.getUserToByUsername(username);
			if(user!=null){
				int idMentioned = user.getIdUser();
				
				//User should not get a notification for mentioning themself
				if(idMentioned==idCurrentUser)
					continue;
				
				//Presistent storage
				postDao.callMentionPostInsert(idCurrentUser,idMentioned, idPost);
				//Push notification Issue #58
				notifierIosImpl.mentionPostNotification(idCurrentUser, idMentioned, idPost);
			}
		}
		
		// Upload image if any
		if (post.getImageFile() != null) {
			if (!imageUtility.isSupportedImageFormat(post.getImageFile()))
				throw new BadImageFormat();

			String imageSrc = idPost
					+ "-"
					+ System.currentTimeMillis()
					+ "."
					+ imageUtility.SUPPORTED_IMAGE_FORMATS.get(post
							.getImageFile().getContentType());
			
			//Save location to database
			if (postDao.insertImage(idPost, imageSrc) != 0) {
				//#83
				//Scaling was causing images to come out oddly sized. 
				//We have the file size limit set, should be good enough.
				//imageUtility.saveImageFileToDisk(
				//		imageUtility.scaleImage(post.getImageFile()), imageSrc,idUser);
						imageUtility.saveImageFileToDisk(post.getImageFile(), imageSrc,idCurrentUser);
				post.setHasImage(true);
			}
		}

		return post.getIdPost() > 0 ? post : null;
	}

	public Post viewPost(int idPost,int idCurrentUser) {
		if (idPost <= 0)
			throw new InvalidPostId();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if ((!serviceUtility.isFollowing(postTo.getIdUser(), idCurrentUser) && serviceUtility
				.isPrivate(postTo))
				|| serviceUtility.isBlocked(idCurrentUser, postTo.getIdUser()))
			throw new UnauthorizedPostRetrieval();
		
		//Issue #29
		if(postTo.getIdRepost()>0)
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());
		
		if (postTo == null)
			throw new InvalidPostId();
		
		return objectTransformUtility.postToToPost(postTo, idCurrentUser);
	}
	
	//Issue #90 isRepost
	public int deletePost(int idPost, int idCurrentUser, boolean isRepost){
		if (idPost <= 0)
			throw new InvalidPostId();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();

		if (!isRepost && postTo.getIdUser() != idCurrentUser)
			throw new UnauthorizedPostRemoval();

		postDao.deleteLikesByIdPost(idPost);
		postDao.deleteCommentsByIdPost(idPost);
		postDao.deleteNotificationsByIdPost(idPost);
		postDao.deletePostHashtagsByIdPost(idPost);
		postDao.deleteImagesByIdPost(idPost);
		
		//Issue #84
		List<PostTo> reposts = postDao.selectPostsByIdRepost(idPost);
		
		for(PostTo repostTo : reposts){
			deletePost(repostTo.getIdPost(),idCurrentUser,true);
		}

		return postDao.deletePostByIdPost(idPost);
	}
	
	public int deletePost(int idPost,int idCurrentUser) {
		return deletePost(idPost,idCurrentUser,false);
	}

	public Post createRepost(int idPost,int idCurrentUser) {
		if (idPost <= 0)
			throw new InvalidPostId();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();
		
		//Issue #87
		if(postTo.getIdRepost()>0)
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());

		if ((serviceUtility.isPrivate(postTo))
				|| serviceUtility.isBlocked(idCurrentUser, postTo.getIdUser()))
			throw new UnauthorizedRepost();

		int originalPoster = postTo.getIdUser();

		// Cannot repost own post
		if (originalPoster == idCurrentUser)
			throw new RepostOwnPost();

		// If repost doesn't exist somehow, maybe this should be handled better
		if (postTo.getIdRepost() > 0) {
			postTo = postDao.selectPostByIdPost(postTo.getIdPost());

			if (postTo == null)
				throw new InvalidPostId();
		}

		// Cannot repost if already reposted by this user
		if (serviceUtility.isReposted(idPost, idCurrentUser))
			throw new AlreadyReposted();

		// Cannot repost expired posts
		if (serviceUtility.isExpired(postTo))
			throw new ExpiredPost();

		postTo.setIdPost(postDao.insertPost("", idCurrentUser, postTo.getIdPost(),
				postTo.getHasImage(), postTo.getExpiration(),
				postTo.getDatestamp()));
		
		//If Repost was successful send insert notifications
		if(postTo.getIdPost() > 0){
			//Insert into presistent datastore
			notificationDao.insertNotification(Notification.NotificationType.REPOST.toString(), postTo.getIdPost(), idCurrentUser, originalPoster);
			//Push notification
			notifierIosImpl.repostNotification(idCurrentUser, originalPoster, idPost);
		}
		
		return postTo.getIdPost() > 0 ? objectTransformUtility.postToToPost(
				postTo, idCurrentUser) : null;
	}

	public Comment createComment(Comment comment, int idPost,int idCurrentUser) {
		if (idPost <= 0)
			throw new InvalidPostId();

		if (comment.getText().replaceAll("[\\n\\s\\t]", "").length() <= 0
				|| comment.getText().replaceAll("[\\n\\s\\t]", "").length() > 500)
			throw new InvalidComment();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();
		
		if ((!serviceUtility.isFollowing(postTo.getIdUser(), idCurrentUser) && serviceUtility
				.isPrivate(postTo))
				|| serviceUtility.isBlocked(idCurrentUser, postTo.getIdUser()))
			throw new UnauthorizedComment();

		if (postTo.getIdRepost() > 0) {
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());
			if (postTo == null)
				throw new InvalidPostId();
		}

		if (serviceUtility.isExpired(postTo))
			throw new ExpiredPost();

		Date datestamp = new Date();
		comment.setDatestamp(datestamp);
		int idComment = postDao.insertComment(comment.getText(), postTo.getIdPost(),
				idCurrentUser, datestamp);
		comment.setIdComment(idComment);

		Set<String> usernameMentions = findMentionsInText(comment.getText());
		
		//TODO: why the fuck do I have to do this
		final int idPost2 = postTo.getIdPost();
	
		// Issue #58 Insert mentions notifications
		for(String username: usernameMentions){
			UserTo user = serviceUtility.getUserToByUsername(username);
			if(user!=null ){
				int idMentioned = user.getIdUser();
				
				//User should not get a notification for mentioning themself
				//Issue #76
				if(idMentioned==idCurrentUser)
					continue;
				
				//Presistent storage
				postDao.callMentionCommentInsert(idCurrentUser,idMentioned, idPost,idComment);
				//Push notification Issue #58
				notifierIosImpl.mentionCommentNotification(idCurrentUser, idMentioned, idPost,idComment);
			}
		}
		
		//Issue #70
		if(postTo.getIdUser()!=idCurrentUser){
			//create comment notification
			int status = notificationDao.insertCommentNotification(Notification.NotificationType.COMMENT.toString(), postTo.getIdPost(), idCurrentUser, postTo.getIdUser(),idComment);
			
			//Issue #58
			if(status > 0)
				notifierIosImpl.commentNotification(idCurrentUser, postTo.getIdUser(), postTo.getIdPost(), idComment);
		}
		return comment.getIdComment() > 0 ? comment : null;

	}

	public List<Comment> getComments(int idPost, int offset, int count,int idCurrentUser) {
		if (idPost <= 0)
			throw new InvalidPostId();

		if (count > COMMENT_COUNT_MAX)
			throw new ExceedsMaxCommentCount();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();

		if ((!serviceUtility.isFollowing(postTo.getIdUser(), idCurrentUser) && serviceUtility
				.isPrivate(postTo))
				|| serviceUtility.isBlocked(idCurrentUser, postTo.getIdUser()))
			throw new UnauthorizedCommentRetrieval();
		
		//Issue #29
		if(postTo.getIdRepost()>0)
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());
		
		if (postTo == null)
			throw new InvalidPostId();
		
		return objectTransformUtility.commentToToComment(postDao
					.selectCommentsByIdPost(postTo.getIdPost(), offset, count));
	}

	public int deleteComment(int idComment, int idPost,int idCurrentUser) {
		if (idPost <= 0)
			throw new InvalidCommentId();

		CommentTo commentTo = postDao.selectCommentByIdComment(idComment);

		if (commentTo == null)
			throw new InvalidCommentId();

		if (commentTo.getIdPost() != idPost) // Comment does not exist for post
			throw new InvalidCommentId();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();

		if (serviceUtility.isExpired(postTo))
			throw new ExpiredPost();

		if (idCurrentUser == commentTo.getIdUser() || postTo.getIdUser() == idCurrentUser) {// Comment Exists
			postDao.deleteNotificationsByIdComment(idComment);
			return postDao.deleteComment(idComment);
		} else {
			throw new UnauthorizedCommentRemoval();
		}
	}

	public int createLike(int idPost,int idCurrentUser) {
		if (idPost <= 0)
			throw new InvalidPostId();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();

		if ((!serviceUtility.isFollowing(postTo.getIdUser(), idCurrentUser) && serviceUtility
				.isPrivate(postTo))
				|| serviceUtility.isBlocked(idCurrentUser, postTo.getIdUser()))
			throw new UnauthorizedLike();

		if (postTo.getIdRepost() > 0)
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());

		if (postDao.countLikeByUserForPost(idCurrentUser, postTo.getIdPost()) > 0)
			throw new AlreadyLiked();

		int sec = 60;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(postTo.getExpiration().getTime());
		cal.add(Calendar.SECOND, sec);
		Timestamp later = new Timestamp(cal.getTime().getTime());
		postTo.setExpiration(later);

		if (serviceUtility.isExpired(postTo))
			throw new ExpiredPost();

		int idLike = postDao.insertLike(postTo.getIdPost(), idCurrentUser);
		int rowPost = postDao.updateExpiration(postTo.getIdPost(),
				postTo.getExpiration());
		
		if(notificationDao.insertLikeNotification(Notification.NotificationType.LIKE.toString(), postTo.getIdPost(), idCurrentUser, postTo.getIdUser(),idLike)>0)
			notifierIosImpl.likeNotification(idCurrentUser, postTo.getIdUser(), idPost, idLike);	//Push notification Issue #58

		return idLike;

	}

	public List<UserExtended> getLikers(int idPost, int offset, int count,int idCurrentUser) {
		if (idPost <= 0)
			throw new InvalidPostId();

		if (count > LIKER_COUNT_MAX)
			throw new ExceedsMaxLikersCount();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();

		if ((!serviceUtility.isFollowing(postTo.getIdUser(), idCurrentUser) && serviceUtility
				.isPrivate(postTo))
				|| serviceUtility.isBlocked(idCurrentUser, postTo.getIdUser()))
			throw new UnauthorizedLikeRetrieval();
		//Issue #29
		if(postTo.getIdRepost()>0)
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());
		
		if (postTo == null)
			throw new InvalidPostId();
		//
		return objectTransformUtility
				.userTosToUserExtended(postDao.selectLikersByIdPost(
						postTo.getIdPost(), offset, count), idCurrentUser);
	}

	private Set<String> findHashtagsInText(String text) {
		if (text == null)
			return null;
		String[] words = text.split(" ");
		Set<String> hashtags = new HashSet<String>();
		for (String word : words) {
			if (word.startsWith("#") && word.length() <= 50) {
				hashtags.add(word);
			}
		}

		return hashtags;
	}

	private Set<String> findMentionsInText(String text) {
		if (text == null)
			return null;
		Set<String> mentions = new HashSet<String>();
		Pattern pattern = Pattern.compile("@([a-zA-Z0-9_]{6,})");
		Matcher matcher = pattern.matcher(text);

		while (matcher.find()) {
			mentions.add(matcher.group(1));
		}

		return mentions;
	}

	private int createHashtag(String hash, int idUser, int idPost) {
		int idHashtags = postDao.selectIdHashtags(hash);
		return idHashtags == 0 ? postDao.insertHashTag(hash) : idHashtags;
	}

/*	private int createMentionPostNotification(String usernameMentioned,
			int idMentioner, int idPost) {
		int idMentioned = postDao.selectIdUserByUsername(usernameMentioned);
		return idMentioned != 0 ? postDao.callMentionPostInsert(idMentioner,
				idMentioned, idPost) : 0;
	}*/

/*	private int createMentionCommentNotification(String usernameMentioned,
			int idMentioner, int idPost, int idComment) {
		int idMentioned = postDao.selectIdUserByUsername(usernameMentioned);
		return idMentioned != 0 ? postDao.callMentionCommentInsert(idMentioner,
				idMentioned, idPost, idComment) : 0;
	}*/

	// TODO figure out a better way to do this
	public String getImageFormat(int idPost, int idCurrentUser) {
		if (idPost < 0)
			throw new InvalidPostId();

		PostTo postTo = postDao.selectPostByIdPost(idPost);

		if (postTo == null)
			throw new InvalidPostId();

		if ((!serviceUtility.isFollowing(postTo.getIdUser(), idCurrentUser) && serviceUtility
				.isPrivate(postTo))
				|| serviceUtility.isBlocked(idCurrentUser, postTo.getIdUser()))
			throw new UnauthorizedImageRetrieval();

		if(postTo.getIdRepost()>0)
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());
		
		if(postTo==null)
			throw new NotFoundPost();
		
		String imageSrc = postDao.selectImageByIdPost(postTo.getIdPost());

		if (imageSrc == null)
			throw new ImageNotFound();

		String extension = imageSrc.substring(imageSrc.lastIndexOf('.') + 1,
				imageSrc.length());
		return imageUtility.getImageType(extension);
	}

	public byte[] getImageBytes(int idPost) {
		PostTo postTo = postDao.selectPostByIdPost(idPost);
		if(postTo.getIdRepost()>0)
			postTo = postDao.selectPostByIdPost(postTo.getIdRepost());
		
		String imageSrc = postDao.selectImageByIdPost(postTo.getIdPost());
		
		return imageSrc != null ? imageUtility.loadImage(postTo.getIdUser()
				+ "/" + imageSrc) : null;
	}

}
