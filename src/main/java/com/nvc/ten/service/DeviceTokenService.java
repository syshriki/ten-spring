package com.nvc.ten.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.DeviceTokenDao;
import com.nvc.ten.exception.InvalidToken;
import com.nvc.ten.models.presistent.DeviceTokenTo;
import com.nvc.ten.utility.ObjectTransformUtility;

@Service
public class DeviceTokenService {
	@Autowired
	DeviceTokenDao deviceTokenDao;
	
	@Autowired
	ObjectTransformUtility objectTransformUtility;
	
	public boolean checkToken(String token,String type, int idCurrentUser){
		if(token==null || token.equals("") || type == null || type.equals(""))
			throw new InvalidToken();
		DeviceTokenTo deviceToken = deviceTokenDao.selectToken(idCurrentUser,type);
		if(deviceToken==null)
			return deviceTokenDao.insertToken(token, idCurrentUser,type)==1;
		if(deviceToken.getToken().equals(token))
			return true;
		else
			return deviceTokenDao.updateToken(token, idCurrentUser,type)==1;
	}
}
