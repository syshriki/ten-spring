package com.nvc.ten.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.StatusDao;
import com.nvc.ten.models.json.Notification;
import com.nvc.ten.utility.ObjectTransformUtility;

@Service
public class StatusService {
	
	@Autowired
	StatusDao statusDao;
	
	@Autowired
	ObjectTransformUtility objectTransformUtility;
	
	//Issue #16
	public List<Notification> getStatus(int offset, int count,int idCurrentUser){
		return objectTransformUtility.notificationTosToNotifications(statusDao.selectStatusByIdUser(idCurrentUser,offset,count),idCurrentUser);
	}

}
