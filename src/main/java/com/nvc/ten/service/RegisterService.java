package com.nvc.ten.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.RegisterDao;
import com.nvc.ten.exception.AlreadyExistsUser;
import com.nvc.ten.models.json.UserExtended;

@Service
public class RegisterService {
	@Autowired
	RegisterDao registerDao;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	public int register(UserExtended user){
		
		//Users can only have one account per email address and no duplicate usernames are allowed
		if(registerDao.countByEmail(user.getEmail()) > 0 || registerDao.countByUsername(user.getUsername())> 0)
			throw new AlreadyExistsUser();
		
		return registerDao.insertUser(user.getUsername(),
				passwordEncoder.encode(user.getPassword()),
				user.getDescription() == null ? "" : user.getDescription(),
				user.getEmail(),
				user.getImageSrc() == null ? "" : user.getImageSrc(),
				user.getWebsite() == null ? "" : user.getWebsite(),
				user.getIsPrivate() ? 1 : 0);
	}
}
