package com.nvc.ten.service;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.AuthDao;
import com.nvc.ten.models.presistent.UserTo;
@Service
public class AuthenticationService implements UserDetailsService {
	@Autowired
	private AuthDao authDao;
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserTo userTo = authDao.selectUser(username);
		
		if(userTo==null)
			return null;
		
		GrantedAuthority authority = new SimpleGrantedAuthority("USER");
		UserDetails userDetails = (UserDetails)new User(userTo.getIdUser()+"", 
				userTo.getPassword(), Arrays.asList(authority));
		return userDetails;
	}

} 