package com.nvc.ten.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.DeviceTokenDao;

@Service
public class LogoutService {
	@Autowired
	DeviceTokenDao deviceTokenDao;
	
	public int logout(int idCurrentUser,String type){
		return deviceTokenDao.deleteToken(idCurrentUser,type);
	}
}
