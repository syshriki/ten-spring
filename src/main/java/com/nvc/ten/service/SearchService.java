package com.nvc.ten.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.SearchDao;
import com.nvc.ten.exception.ExceedsMaxSearchHashtagsCount;
import com.nvc.ten.exception.ExceedsMaxSearchUsersCount;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.utility.ObjectTransformUtility;
import com.nvc.ten.utility.ServiceUtility;

@Service
public class SearchService {
	
	@Autowired
	SearchDao searchDao;
	
	@Autowired
	ServiceUtility serviceUtility;

	@Autowired
	ObjectTransformUtility objectTransformUtility;
	
	@Value("${search.user.count.max}")
	private int USER_COUNT_MAX;
	
	@Value("${search.hashtag.count.max}")
	private int HASHTAG_COUNT_MAX;
	
	private boolean returnImages;
	
	public List<UserExtended> searchUsers(String username,int offset,int count,int idCurrentUser){
		if(count>USER_COUNT_MAX)
			throw new ExceedsMaxSearchUsersCount();
		
		return objectTransformUtility.userTosToUserExtended(searchDao.selectUsersByLikeUsername(idCurrentUser,username, offset, count),idCurrentUser);
	}
	
	public List<Post> searchHashtags(String hashtag,int offset,int count,int idCurrentUser){
		if(count>HASHTAG_COUNT_MAX)
			throw new ExceedsMaxSearchHashtagsCount();
		
		return objectTransformUtility.postTosToPosts(searchDao.selectPostsByLikeHashtag(idCurrentUser,hashtag, offset, count),idCurrentUser);
	}
	
	public boolean isReturnImages() {
		return returnImages;
	}

	public void setReturnImages(boolean returnImages) {
		this.returnImages = returnImages;
	}
}

