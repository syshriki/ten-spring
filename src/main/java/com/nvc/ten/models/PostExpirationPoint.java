package com.nvc.ten.models;

import java.util.Date;

import com.nvc.ten.models.presistent.PostTo;

public class PostExpirationPoint extends PostTo implements Comparable<PostExpirationPoint>{

	@Override
	public int compareTo(PostExpirationPoint y) {
		if (this.getExpiration().before(y.getExpiration()))
        {
            return 1;
        }
        if (y.getExpiration().before(this.getExpiration()))
        {
            return -1;
        }
        return 0;
	}
	
	@Override
	public int hashCode(){
		return this.getIdPost()*37;
	}
}
