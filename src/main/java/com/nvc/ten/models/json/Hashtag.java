package com.nvc.ten.models.json;


public class Hashtag {
	private Integer hashtagid;
	private String hashtag;
	
	public Hashtag() {
	}
	public Integer getHashtagid() {
		return hashtagid;
	}
	public void setHashtagid(Integer hashtagid) {
		this.hashtagid = hashtagid;
	}
	public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
}
