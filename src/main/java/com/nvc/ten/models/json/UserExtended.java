package com.nvc.ten.models.json;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nvc.ten.validator.Password;
import com.nvc.ten.validator.Username;

public class UserExtended {
	@Password
	private String password;
	@Username
	private String username;
	private Integer idUser;
	private String imageSrc;
	@Size(min = 0, max = 191, message = "Description too long")
	private String description;
	@Email(message = "Invalid email address")
	private String email;
	private String website;
	private boolean isPrivate;
	private boolean isBlocked;
	private Friended friended;
	
	public UserExtended() {
		// TODO Auto-generated constructor stub
	}
	
	public UserExtended(String username) {
		this.username = username;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public Friended getFriended() {
		return friended;
	}
	public void setFriended(Friended friended) {
		this.friended = friended;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	public String getImageSrc() {
		return imageSrc;
	}
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	@JsonIgnore //Dont want this ever getting returned
	public String getPassword() {
		return password;
	}

	@JsonProperty  //We want automatic deserialization though from the controller
	public void setPassword(String password) {
		this.password = password;
	}
	
	public enum Friended {
		 NONE(-2),REJECTED(-1), PENDING(0), FRIENDED(1), SELF(2);
		 
		 private int code;
		 
		 private Friended(int code) {
		   this.code = code;
		 }
		 
		 public int getCode() {
		   return code;
		 }
		 private boolean Compare(int code){return this.code == code;}
		 public static Friended GetValue(int code)
	        {
			 	Friended[] As = Friended.values();
	            for(int i = 0; i < As.length; i++)
	            {
	                if(As[i].Compare(code))
	                    return As[i];
	            }
	            return Friended.NONE;
	        }
	}
}
