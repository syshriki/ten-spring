package com.nvc.ten.models.json;

import java.util.List;

public class Profile {
	private List<User> likers;
	private List<Post> activePosts;
	private Post bestTimedPost;
 	private UserExtended user;
 	private int likeCount;
 	private int followersCount;
 	private int followingCount;
 	private int commentCount;
 	private int allPostCount;
	public List<User> getLikers() {
		return likers;
	}
	public void setLikers(List<User> likers) {
		this.likers = likers;
	}
	public List<Post> getActivePosts() {
		return activePosts;
	}
	public void setActivePosts(List<Post> activePosts) {
		this.activePosts = activePosts;
	}
	public Post getBestTimedPost() {
		return bestTimedPost;
	}
	public void setBestTimedPost(Post bestTimedPost) {
		this.bestTimedPost = bestTimedPost;
	}
	public UserExtended getUser() {
		return user;
	}
	public void setUser(UserExtended user) {
		this.user = user;
	}
	public int getLikeCount() {
		return likeCount;
	}
	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}
	public int getFollowersCount() {
		return followersCount;
	}
	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}
	public int getFollowingCount() {
		return followingCount;
	}
	public void setFollowingCount(int followingCount) {
		this.followingCount = followingCount;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public int getAllPostCount() {
		return allPostCount;
	}
	public void setAllPostCount(int allPostCount) {
		this.allPostCount = allPostCount;
	}
}
