package com.nvc.ten.models.json;

import java.util.Date;

public class Notification {
	private int idNotification;
	private Date datestamp;
	private Post post;
	private User user;
	private String message;
	private NotificationType type;
	
	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getIdNotification() {
		return idNotification;
	}

	public void setIdNotification(int idNotification) {
		this.idNotification = idNotification;
	}
	
	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public enum NotificationType {
		 COMMENT,LIKE,MENTIONPOST,MENTIONCOMMENT,FRIENDREQUEST,FOLLOW,FOLLOWING,REPOST,POSTEXPIRED;
	}
}
