package com.nvc.ten.models.json;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {
	private String username;
	private Integer idUser;
	private String imageSrc;
	private boolean isPrivate;
	
	public User() {

	}

	public User(String username, Integer idUser, String imageSrc) {
		this.username = username;
		this.idUser = idUser;
		this.imageSrc = imageSrc;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getImageSrc() {
		return imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
}
