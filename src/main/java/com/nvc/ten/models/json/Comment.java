package com.nvc.ten.models.json;

import java.util.Date;

import javax.validation.constraints.Size;

public class Comment {
	private User user;
	@Size(min = 0, max = 5000, message = "Comment body is too long")
	private String text;
	private int idComment;
	private Date datestamp;
	 public Comment(){
		 
	 }
	 
	 public Comment(String text){
		 
	 }
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getIdComment() {
		return idComment;
	}
	public void setIdComment(int idComment) {
		this.idComment = idComment;
	}
	public Date getDatestamp() {
		return datestamp;
	}
	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
