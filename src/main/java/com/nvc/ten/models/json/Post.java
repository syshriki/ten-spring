package com.nvc.ten.models.json;

import java.util.Date;

import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Post {
	private Integer idPost;
	@Size(min = 0, max = 500, message = "Text body is too long")
	private String text;
	private Date datestamp;
	private Date expiration;
	private Date originalDatestamp;
	//TODO get rid of image and imageFile
	private String image;
	@JsonIgnore
	private MultipartFile imageFile;
	private User userPoster;
	private User userReposter;
	private boolean isLiked;
	private boolean isCommented;
	private boolean isReposted;
	private boolean hasImage;

	public boolean isLiked() {
		return isLiked;
	}

	public void setLiked(boolean isLiked) {
		this.isLiked = isLiked;
	}

	public boolean isCommented() {
		return isCommented;
	}

	public void setCommented(boolean isCommented) {
		this.isCommented = isCommented;
	}

	public Post() {
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getText() {
		return text;
	}

	public User getUserReposter() {
		return userReposter;
	}

	public void setUserReposter(User userReposter) {
		this.userReposter = userReposter;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	@JsonIgnore
	// Dont want this ever getting returned
	public MultipartFile getImageFile() {
		return imageFile;
	}

	@JsonProperty
	// We want automatic deserialization though from the controller
	public void setImageFile(MultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Integer getIdPost() {
		return idPost;
	}

	public void setIdPost(Integer idPost) {
		this.idPost = idPost;
	}

	public User getUserPoster() {
		return userPoster;
	}

	public void setUserPoster(User userPoster) {
		this.userPoster = userPoster;
	}

	public boolean isHasImage() {
		return hasImage;
	}

	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}

	public boolean getIsReposted() {
		return isReposted;
	}

	public void setIsReposted(boolean isReposted) {
		this.isReposted = isReposted;
	}

	public Date getOriginalDatestamp() {
		return originalDatestamp;
	}

	public void setOriginalDatestamp(Date originalDatestamp) {
		this.originalDatestamp = originalDatestamp;
	}

}
