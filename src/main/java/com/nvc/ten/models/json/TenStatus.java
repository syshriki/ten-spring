package com.nvc.ten.models.json;

public class TenStatus {
	private boolean success;
	
	public TenStatus(boolean success) {
		this.success = success;
	}
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}
