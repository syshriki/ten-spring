package com.nvc.ten.models.presistent;

import java.util.Date;


public class PasswordResetToken {
	private int id;

	private String token;

	private int idUser;
	
	private Date expiryDate;

	public PasswordResetToken(){
		
	}
	public PasswordResetToken(String token, int idUser){
		this.token = token;
		this.idUser = idUser;
	}
	private boolean isUsed;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public boolean getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
}
