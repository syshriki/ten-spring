package com.nvc.ten.models.presistent;

import java.util.Date;

public class BlockTo {
	private int idBlock;
	private int idBlocked;
	private int idBlocker;
	private Date datestamp;

	public BlockTo() {
		// TODO Auto-generated constructor stub
	}

	public int getIdBlock() {
		return idBlock;
	}

	public void setIdBlock(int idBlock) {
		this.idBlock = idBlock;
	}

	public int getIdBlocked() {
		return idBlocked;
	}

	public void setIdBlocked(int idBlocked) {
		this.idBlocked = idBlocked;
	}

	public int getIdBlocker() {
		return idBlocker;
	}

	public void setIdBlocker(int idBlocker) {
		this.idBlocker = idBlocker;
	}

	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}
}
