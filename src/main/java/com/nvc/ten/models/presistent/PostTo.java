package com.nvc.ten.models.presistent;

import java.util.Date;

public class PostTo {
	private int idPost;
	private int idUser;
	private String text;
	private int idRepost;
	private Date datestamp;
	private Date expiration;
	private int hasImage;

	public int getHasImage() {
		return hasImage;
	}
	
	public void setHasImage(int hasImage) {
		this.hasImage = hasImage;
	}
	
	public PostTo() {
		// TODO Auto-generated constructor stub
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getIdRepost() {
		return idRepost;
	}

	public void setIdRepost(int idRepost) {
		this.idRepost = idRepost;
	}

	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}
}
