package com.nvc.ten.models.presistent;

import java.sql.Timestamp;

public class UserTo {
	private int idUser;
	private String username;
	private String password;
	private String description = "";
	private String email;
	private String imageSrc = "";
	private String website = "";
	private boolean isPrivate;
	private Timestamp datestamp;
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImageSrc() {
		return imageSrc;
	}
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public boolean isPrivate() {
		return isPrivate;
	}
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	public Timestamp getDatestamp() {
		return datestamp;
	}
	public void setDatestamp(Timestamp datestamp) {
		this.datestamp = datestamp;
	}

}
