package com.nvc.ten.models.presistent;

import java.util.Date;

public class NotificationTo {
	private Date datestamp;
	private String notificationType;
	private int id; //TODO idPost
	private int idComment;
	private int idLike;
	private int idAffected;
	private int idAffecting;
	private int idNotification;
	public Date getDatestamp() {
		return datestamp;
	}
	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdAffected() {
		return idAffected;
	}
	public void setIdAffected(int idAffected) {
		this.idAffected = idAffected;
	}
	public int getIdAffecting() {
		return idAffecting;
	}
	public void setIdAffecting(int idAffecting) {
		this.idAffecting = idAffecting;
	}
	public int getIdNotification() {
		return idNotification;
	}
	public void setIdNotification(int idNotification) {
		this.idNotification = idNotification;
	}
	public int getIdComment() {
		return idComment;
	}
	public void setIdComment(int idComment) {
		this.idComment = idComment;
	}
	public int getIdLike() {
		return idLike;
	}
	public void setIdLike(int idLike) {
		this.idLike = idLike;
	}
}
