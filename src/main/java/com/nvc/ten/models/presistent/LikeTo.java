package com.nvc.ten.models.presistent;

import java.util.Date;

public class LikeTo {
	private Date datestamp;
	private int idLike;
	private int idPost;
	private int idUser;

	public LikeTo(int idUser, int idUserPost) {
		this.idUser = idUser;
		this.idPost = idUserPost;
	}

	public LikeTo() {
		// TODO Auto-generated constructor stub
	}

	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getIdLike() {
		return idLike;
	}

	public void setIdLike(int idLike) {
		this.idLike = idLike;
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}
}
