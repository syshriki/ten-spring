package com.nvc.ten.models.presistent;

import java.util.Date;

public class PostHashtagTo {
	private int idPostHashtag;
	private int idPost;
	private int idHashtag;
	private Date datestamp;
	public int getIdPostHashtag() {
		return idPostHashtag;
	}
	public void setIdPostHashtag(int idPostHashtag) {
		this.idPostHashtag = idPostHashtag;
	}
	public int getIdPost() {
		return idPost;
	}
	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}
	public int getIdHashtag() {
		return idHashtag;
	}
	public void setIdHashtag(int idHashtag) {
		this.idHashtag = idHashtag;
	}
	public Date getDatestamp() {
		return datestamp;
	}
	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}
}
