package com.nvc.ten.models.presistent;

import java.util.Date;

public class ImageTo {
	private int idImage;
	private String imageSrc;
	private int idPost;
	private Date datestamp;
	
	public ImageTo() {
		// TODO Auto-generated constructor stub
	}

	public int getIdImage() {
		return idImage;
	}

	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}

	public String getImageSrc() {
		return imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}
}
