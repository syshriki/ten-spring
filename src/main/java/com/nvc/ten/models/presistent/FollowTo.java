package com.nvc.ten.models.presistent;

import java.util.Date;

public class FollowTo {
	private int idFollowers;
	private int idRequestee;
	private int idRecipient;
	private int approved;
	private Date datestamp;

	public int getIdFollowers() {
		return idFollowers;
	}

	public void setIdFollowers(int idFollowers) {
		this.idFollowers = idFollowers;
	}

	public int getIdRequestee() {
		return idRequestee;
	}

	public void setIdRequestee(int idRequestee) {
		this.idRequestee = idRequestee;
	}

	public int getIdRecipient() {
		return idRecipient;
	}

	public void setIdRecipient(int idRecipient) {
		this.idRecipient = idRecipient;
	}

	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public enum FollowType{
		REJETED(-1),PENDING(0),ACCEPTED(1);
		private int id;
		private FollowType(int integerRepresentation){
			this.id = integerRepresentation;
		}
		
		public int getId(){
			return this.id;
		}
		
	}
}
