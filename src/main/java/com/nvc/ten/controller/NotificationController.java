package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nvc.ten.models.json.Notification;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.TenStatus;
import com.nvc.ten.service.NotificationService;
import com.nvc.ten.utility.ObjectTransformUtility;

@Controller
public class NotificationController {
	@Autowired
	NotificationService notificationService;

	@RequestMapping(path = "/notification", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Notification>> notifications(
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${notification.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections
				.unmodifiableMap(new HashMap<String, List<Notification>>() {
					{
						put("notifications", notificationService.getNotifications(
								offset, count,idCurrentUser));
					}
				});
	}

	@RequestMapping(path = "/notification/count", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Integer> postCount() {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String, Integer>() {
			{
				put("count", notificationService.getNotificationCount(idCurrentUser));
			}
		});
	}
	
	//Issue #24
	@RequestMapping(path = "/notification/{idNotification}", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus readNotification(@PathVariable int idNotification) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(notificationService.readNotification(idNotification,idCurrentUser)>0);
	}
}
