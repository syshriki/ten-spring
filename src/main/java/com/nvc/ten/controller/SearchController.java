package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.service.SearchService;
import com.nvc.ten.validator.Username;

@Controller
public class SearchController {
	@Autowired
	SearchService searchService;

	@RequestMapping(path = "/search/user/{username}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<UserExtended>> searchUsers(
			@PathVariable String username,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${search.user.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections
				.unmodifiableMap(new HashMap<String, List<UserExtended>>() {
					{
						put("users", searchService.searchUsers(username,
								offset, count,idCurrentUser));
					}
				});
	}

	@RequestMapping(path = "/search/post/{hashtag}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Post>> searchPosts(
			@PathVariable String hashtag,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${search.hashtag.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String, List<Post>>() {
			{
				put("posts",
						searchService.searchHashtags(hashtag, offset, count,idCurrentUser));
			}
		});
	}
}
