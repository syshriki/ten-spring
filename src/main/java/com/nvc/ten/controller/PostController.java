package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nvc.ten.exception.InternalException;
import com.nvc.ten.exception.InvalidImage;
import com.nvc.ten.models.json.Comment;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.TenException;
import com.nvc.ten.models.json.TenStatus;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.service.PostService;

@RestController
public class PostController {
	@Autowired
	PostService postService;

	@RequestMapping(path = "/post", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Post> post(@Valid Post post) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());		
		return Collections.unmodifiableMap(new HashMap<String, Post>() {
			{
				put("post", postService.createPost(post,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/post/{idPost}/repost", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus repost(@PathVariable int idPost) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());		
		return new TenStatus(postService.createRepost(idPost,idCurrentUser)!=null);
	}
	
	@RequestMapping(path = "/post/{idPost}/like", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus like(@PathVariable int idPost) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());		
		int idLike = postService.createLike(idPost,idCurrentUser);
		return new TenStatus(idLike>0);
	}
	
	@RequestMapping(path = "/post/{idPost}/comment", method = RequestMethod.PUT)
	@ResponseBody
	public Map<String, Comment> comment(@PathVariable int idPost,@RequestBody Comment comment) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());		
		return Collections.unmodifiableMap(new HashMap<String, Comment>() {
			{
				put("comment", postService.createComment(comment,idPost,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/post/{idPost}/comment/{idComment}", method = RequestMethod.DELETE)
	@ResponseBody
	public TenStatus deleteComment(@PathVariable int idPost,@PathVariable int idComment) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int rows = postService.deleteComment(idComment,idPost,idCurrentUser);
		return new TenStatus(rows>0);
	}
	
	@RequestMapping(path = "/post/{idPost}", method = RequestMethod.DELETE)
	@ResponseBody
	public TenStatus deletePost(@PathVariable int idPost) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int rows = postService.deletePost(idPost,idCurrentUser);
		return new TenStatus(rows>0);
	}
	
	@RequestMapping(path = "/post/{idPost}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Post> view(@PathVariable int idPost) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		Post post = postService.viewPost(idPost,idCurrentUser);
		return Collections.unmodifiableMap(new HashMap<String, Post>() {
			{
				put("post", post);
			}
		});
	}
	
	@RequestMapping(path = "/post/{idPost}/comment", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Comment>> comments(
			@PathVariable int idPost,
			@RequestParam(required=false,defaultValue="0")int offset,
			@RequestParam(required=false,defaultValue="${post.comment.count.default}")int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String, List<Comment>>() {
			{
				put("comments", postService.getComments(idPost, offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/post/{idPost}/like", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<UserExtended>> likers(
			@PathVariable int idPost,
			@RequestParam(required=false,defaultValue="0")int offset,
			@RequestParam(required=false,defaultValue="${post.liker.count.default}")int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String, List<UserExtended>>() {
			{
				put("users", postService.getLikers(idPost, offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(value = "/post/{idPost}/image", method = RequestMethod.GET)
	  public void postImage(HttpServletResponse response,@PathVariable int idPost) throws Exception {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		String imageFormat = postService.getImageFormat(idPost,idCurrentUser);
		byte [] imageBytes = postService.getImageBytes(idPost);
		if(imageBytes==null || imageFormat==null)
			throw new InvalidImage();
	    response.setHeader("Cache-Control", "no-store");
	    response.setHeader("Pragma", "no-cache");
	    response.setDateHeader("Expires", 0);
	    response.setContentType(imageFormat);
	    ServletOutputStream responseOutputStream = response.getOutputStream();
	    responseOutputStream.write(imageBytes);
	    responseOutputStream.flush();
	    responseOutputStream.close();
	  }
	
	@ExceptionHandler(BindException.class)
	public ResponseEntity<TenException> processValidationError(BindException ex) {
		TenException te = new TenException(
				org.springframework.http.HttpStatus.BAD_REQUEST.value(),
				org.springframework.http.HttpStatus.BAD_REQUEST.name(),
				ex.getFieldError().getDefaultMessage());
		return new ResponseEntity<TenException>(te,org.springframework.http.HttpStatus.BAD_REQUEST);
	}
	
}
