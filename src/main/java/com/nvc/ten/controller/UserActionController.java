package com.nvc.ten.controller;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nvc.ten.models.json.TenStatus;
import com.nvc.ten.models.json.User;
import com.nvc.ten.service.UserActionService;

@Controller
public class UserActionController {
	@Autowired
	UserActionService userActionService;
	
	@RequestMapping(value = "/action/forgotPassword", method = RequestMethod.GET)
	@ResponseBody
	public TenStatus resetPassword(@RequestParam("email") String email) {
	    return new TenStatus(userActionService.emailResetToken(email));
	}
	
	@RequestMapping(value = "/action/resetPassword", method = RequestMethod.GET)
	@ResponseBody
	public User changePassword(@RequestParam("email") String email, @RequestParam("token") String token, @RequestParam("password") String password) {
	    return userActionService.resetPassword(token,email,password);
	}
	
	@RequestMapping(path = "/action/{idUser}/approve", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus approve(@PathVariable int idUser) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(userActionService.approveFollowRequest(idUser,idCurrentUser)>0);
	}
	
	@RequestMapping(path = "/action/pending", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,List<User>> pending(@RequestParam(required=false,defaultValue="0")int offset,
			@RequestParam(required=false,defaultValue="${post.comment.count.default}")int count) {

		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String, List<User>>() {
			{
				put("users", userActionService.getPendingRequests(offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/action/{idUser}/reject", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus reject(@PathVariable int idUser) {

		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(userActionService.rejectFollowRequest(idUser,idCurrentUser)>0);
	}
	
	@RequestMapping(path = "/action/{idUser}/follow", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus follow(@PathVariable int idUser) {

		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(userActionService.followUser(idUser,idCurrentUser)>0);
	}
	
	@RequestMapping(path = "/action/{idUser}/unfollow", method = RequestMethod.DELETE)
	@ResponseBody
	public TenStatus unfollow(@PathVariable int idUser) {

		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(userActionService.unfollowUser(idUser,idCurrentUser)>0);
	}
	
	@RequestMapping(path = "/action/{idUser}/cancel", method = RequestMethod.DELETE)
	@ResponseBody
	public TenStatus cancelPending(@PathVariable int idUser) {

		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(userActionService.cancelPendingRequests(idUser) > 0);
	}
	
	@RequestMapping(path = "/action/{idUser}/block", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus block(@PathVariable int idUser) {

		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(userActionService.blockUser(idUser,idCurrentUser)>0);
	}
	
	@RequestMapping(path = "/action/{idUser}/unblock", method = RequestMethod.DELETE)
	@ResponseBody
	public TenStatus unblock(@PathVariable int idUser) {

		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(userActionService.unblockUser(idUser,idCurrentUser)>0);
	}
		
}
