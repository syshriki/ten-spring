package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.User;
import com.nvc.ten.service.PopularService;

@Controller
public class PopularController {
	@Autowired
	PopularService popularService;

	@RequestMapping(path = "/popular/timer/local", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<User>> popularTimerLocal(
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${popular.timer.local.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections.unmodifiableMap(new HashMap<String, List<User>>() {
			{
				put("users", popularService.getToptimersLocal(offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/popular/timer/global", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<User>> popularTimerGlobal(
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${popular.timer.global.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections.unmodifiableMap(new HashMap<String, List<User>>() {
			{
				put("users", popularService.getToptimersGlobal(offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/popular/timed/local", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Post>> popularTimedLocal(
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${popular.timed.local.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections.unmodifiableMap(new HashMap<String, List<Post>>() {
			{
				put("posts",popularService.getToptimedLocal(offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/popular/timed/global", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Post>> popularTimedGlobal(
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${popular.timed.global.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections.unmodifiableMap(new HashMap<String, List<Post>>() {
			{
				put("posts",popularService.getToptimedGlobal(offset, count,idCurrentUser));
			}
		});
	}
}
