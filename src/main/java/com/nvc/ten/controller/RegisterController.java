package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nvc.ten.exception.InvalidCredential;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.service.RegisterService;

@RestController
public class RegisterController {
	@Autowired
	private RegisterService registerService;

	@RequestMapping(path = "/register", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, UserExtended> register(@Valid @RequestBody UserExtended user,BindingResult result) {
		if(result.hasErrors()){
			throw new InvalidCredential(result.getFieldError().getDefaultMessage());
		}
		user.setIdUser(registerService.register(user));
		return Collections.unmodifiableMap(new HashMap<String, UserExtended>() {
			{
				put("user", user);
			}
		});
	}
}
