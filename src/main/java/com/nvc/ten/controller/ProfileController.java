package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nvc.ten.exception.InternalException;
import com.nvc.ten.exception.InvalidCredential;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.Profile;
import com.nvc.ten.models.json.TenStatus;
import com.nvc.ten.models.json.User;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.service.ProfileService;

@Controller
public class ProfileController {
	
	@Autowired
	ProfileService profileService;
	
	@RequestMapping(path = "/profile/{username}/post/active", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Post>> activePosts(
			@PathVariable String username,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${profile.posts.active.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections.unmodifiableMap(new HashMap<String, List<Post>>() {
			{
				put("posts",
						profileService.getActivePosts(username, offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/profile/{username}/post", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Post>> allPosts(
			@PathVariable String username,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${profile.posts.all.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections.unmodifiableMap(new HashMap<String, List<Post>>() {
			{
				put("posts",
						profileService.getAllPosts(username, offset, count,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(value = "/profile/{username}/image", method = RequestMethod.GET)
	  public void profileImage(HttpServletResponse response,@PathVariable String username) throws Exception {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		String imageFormat = profileService.getImageFormat(username,idCurrentUser);
		byte [] imageBytes = profileService.getImageBytes(username);
		if(imageBytes==null || imageFormat==null)
			throw new InternalException();
	    response.setHeader("Cache-Control", "no-store");
	    response.setHeader("Pragma", "no-cache");
	    response.setDateHeader("Expires", 0);
	    response.setContentType(imageFormat);
	    ServletOutputStream responseOutputStream = response.getOutputStream();
	    responseOutputStream.write(imageBytes);
	    responseOutputStream.flush();
	    responseOutputStream.close();
	  }
	
	@RequestMapping(path = "/profile/{username}/post/top", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Post> bestTimed(@PathVariable String username) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String, Post>() {
			{
				put("post", profileService.getBestTimedPost(username,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/profile/{username}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Profile> profile(@PathVariable String username) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());		return Collections.unmodifiableMap(new HashMap<String, Profile>() {
			{
				put("profile",profileService.getProfile(username,idCurrentUser));
			}
		});
	}
	
	@RequestMapping(path = "/profile/{idUser}/following", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<UserExtended>> following(
			@PathVariable int idUser,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${profile.following.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections
				.unmodifiableMap(new HashMap<String, List<UserExtended>>() {
					{
						put("users", profileService.getFollowing(idUser, offset, count,idCurrentUser));
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/following/search/{username}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<UserExtended>> findFollowing(
			@PathVariable int idUser,
			@PathVariable String username,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${profile.following.search.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections
				.unmodifiableMap(new HashMap<String, List<UserExtended>>() {
					{
						put("users", profileService.findFollowing(idUser, username, offset, count,idCurrentUser));
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/followers", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<UserExtended>> followers(
			@PathVariable int idUser,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${profile.followers.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections
				.unmodifiableMap(new HashMap<String, List<UserExtended>>() {
					{
						put("users", profileService.getFollowers(idUser, offset, count,idCurrentUser));
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/followers/search/{username}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<UserExtended>> findFollowers(
			@PathVariable int idUser,
			@PathVariable String username,
			@RequestParam(required = false, defaultValue = "0") int offset,
			@RequestParam(required = false, defaultValue = "${profile.followers.search.count.default}") int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());

		return Collections
				.unmodifiableMap(new HashMap<String, List<UserExtended>>() {
					{
						put("users", profileService.findFollowers(idUser, username, offset, count,idCurrentUser));
					}
				});
	}
	
	@RequestMapping(path = "/profile", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus updateProfile(@Valid @RequestBody UserExtended user,BindingResult result) {
		if(result.hasErrors()){
			throw new InvalidCredential(result.getFieldError().getDefaultMessage());
		}
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int rows = profileService.updateProfile(user,idCurrentUser);
		return new TenStatus(rows>0);
	}
	
	@RequestMapping(path = "/profile/pass", method = RequestMethod.PUT)
	@ResponseBody
	public TenStatus updatePassword(@Valid @RequestBody UserExtended user,BindingResult result) {
		if(result.hasErrors()){
			throw new InvalidCredential(result.getFieldError().getDefaultMessage());
		}
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int rows = profileService.updatePassword(user,idCurrentUser);
		return new TenStatus(rows>0);
	}
	
	@RequestMapping(path = "/profile/image", method = RequestMethod.POST)
	@ResponseBody
	public TenStatus update(MultipartFile imageFile) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int rows = profileService.updateProfileImage(imageFile,idCurrentUser);
		return new TenStatus(rows>0);
	}
	
	@RequestMapping(path = "/profile/{username}/like/top", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<User>> topLikers(@PathVariable String username) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections
				.unmodifiableMap(new HashMap<String, List<User>>() {
					{
						put("users", profileService.getTopLikers(username,idCurrentUser));
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/post/count", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Integer> postCount(@PathVariable int idUser) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int count = profileService.countAllPosts(idUser);
		return Collections
				.unmodifiableMap(new HashMap<String,Integer>() {
					{
						put("count", count);
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/following/count", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Integer> followingCount(@PathVariable int idUser) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int count = profileService.countFollwing(idUser);
		return Collections
				.unmodifiableMap(new HashMap<String,Integer>() {
					{
						put("count", count);
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/follower/count", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Integer> followerCount(@PathVariable int idUser) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int count = profileService.countFollwers(idUser);
		return Collections
				.unmodifiableMap(new HashMap<String,Integer>() {
					{
						put("count", count);
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/like/count", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Integer> likeCount(@PathVariable int idUser) {
		int count = profileService.countLikes(idUser);
		return Collections
				.unmodifiableMap(new HashMap<String,Integer>() {
					{
						put("count", count);
					}
				});
	}
	
	@RequestMapping(path = "/profile/{idUser}/comment/count", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Integer> commentCount(@PathVariable int idUser) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		int count = profileService.countComments(idUser);
		return Collections
				.unmodifiableMap(new HashMap<String,Integer>() {
					{
						put("count", count);
					}
				});
	}
}
