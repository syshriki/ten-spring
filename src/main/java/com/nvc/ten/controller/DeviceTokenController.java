package com.nvc.ten.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nvc.ten.models.json.TenStatus;
import com.nvc.ten.service.DeviceTokenService;

@RestController
public class DeviceTokenController {
	@Autowired
	DeviceTokenService deviceTokenService;

	@RequestMapping(path = "/token", method = RequestMethod.GET)
	public TenStatus login(@RequestParam("token") String token,@RequestParam("type") String type) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(deviceTokenService.checkToken(token,type,idCurrentUser));
	}
}
