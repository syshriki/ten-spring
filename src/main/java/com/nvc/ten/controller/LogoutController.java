package com.nvc.ten.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nvc.ten.models.json.TenStatus;
import com.nvc.ten.service.LogoutService;

@RestController
public class LogoutController {
	@Autowired
	LogoutService logoutService;

	@RequestMapping(path = "/unregister/{device}", method = RequestMethod.GET)
	public TenStatus logout(@PathVariable String device) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return new TenStatus(logoutService.logout(idCurrentUser,device)>0);
	}
}
