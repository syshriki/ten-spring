package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nvc.ten.models.json.Post;
import com.nvc.ten.service.NewsfeedService;

@RestController
public class NewsfeedController {
	//TODO make all methods use dependency container
	@Autowired
	NewsfeedService newsfeedService;
	
    @RequestMapping(path = "/newsfeed", method = RequestMethod.GET)
    public Map<String,List<Post>> feed(@RequestParam(required=false,defaultValue="0")int offset,
			@RequestParam(required=false,defaultValue="${newsfeed.count.default}")int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String, List<Post>>() {
			{
				put("posts", newsfeedService.getNewsfeed(offset, count,idCurrentUser));
			}
		});
    }
}
