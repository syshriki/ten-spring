package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nvc.ten.models.json.User;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.service.LoginService;

@RestController
public class LoginController {
	@Autowired
	LoginService loginService;

	@RequestMapping(path = "/login", method = RequestMethod.GET)
	public Map<String, UserExtended> login() {
		return Collections.unmodifiableMap(new HashMap<String, UserExtended>() {
			{
				put("user", loginService.login(Integer.parseInt(SecurityContextHolder.getContext().getAuthentication().getName())));
			}
		});
	}
}
