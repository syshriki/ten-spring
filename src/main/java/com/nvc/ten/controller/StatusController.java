package com.nvc.ten.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nvc.ten.models.json.Notification;
import com.nvc.ten.service.StatusService;

@Controller
public class StatusController {
	@Autowired
	StatusService statusService;
	
	@RequestMapping(path = "/status", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<Notification>> activePosts(@RequestParam(required = false, defaultValue = "0")int offset, 
			@RequestParam(required = false, defaultValue = "${status.count.default}")int count) {
		int idCurrentUser = Integer.parseInt(SecurityContextHolder
				.getContext().getAuthentication().getName());
		return Collections.unmodifiableMap(new HashMap<String,List<Notification>>() {
			{
				put("notifications",statusService.getStatus(offset,count,idCurrentUser));
			}
		});
	}
	
}
