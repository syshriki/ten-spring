package com.nvc.ten.test;

import org.springframework.web.multipart.MultipartFile;

import com.nvc.ten.models.json.UserExtended;

public class TUser extends UserExtended{
	public int create(){
		int result = 0;
		return result;
	}
	
	public int post(String text){
		int result = 0;
		return result;
	}
	
	public int post(String text,MultipartFile image){
		int result = 0;
		return result;
	}
	
	public int follow(int idUser){
		int result = 0;
		return result;
	}
	
	public int block(int idUser){
		int result = 0;
		return result;
	}
	
	public int unblock(int idUser){
		int result = 0;
		return result;
	}
	
	public int approve(int idUser){
		int result = 0;
		return result;
	}
	
	public int reject(int idUser){
		int result = 0;
		return result;
	}
	
	public int comment(int idPost, String text){
		int result = 0;
		return result;
	}
	
	public int unfollow(int idUser){
		int result = 0;
		return result;
	}
	
	public int like(int idPost){
		int result = 0;
		return result;
	}
	
	public int repost(int idPost){
		int result = 0;
		return result;
	}
	
	public int deletePost(int idPost){
		int result = 0;
		return result;
	}
	
	public int deleteComment(int idComment){
		int result = 0;
		return result;
	}
}
