package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.UNAUTHORIZED,reason="Cannot view post")
public class UnauthorizedPostRetrieval extends RuntimeException{

}
