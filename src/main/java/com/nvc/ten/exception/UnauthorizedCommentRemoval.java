package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.UNAUTHORIZED,reason="Could not remove comment")
public class UnauthorizedCommentRemoval extends RuntimeException{

}
