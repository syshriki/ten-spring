package com.nvc.ten.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason="Max comment count exceeded ")
public class ExceedsMaxCommentCount extends RuntimeException {

}