package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.FORBIDDEN,reason="Cannot repost own post")
public class RepostOwnPost extends RuntimeException{

}
