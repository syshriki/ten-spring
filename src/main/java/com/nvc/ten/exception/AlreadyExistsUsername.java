package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST,reason="Username already used")
public class AlreadyExistsUsername extends RuntimeException{

}
