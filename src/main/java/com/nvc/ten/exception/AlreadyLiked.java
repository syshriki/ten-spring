package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.FORBIDDEN,reason="Already liked post")
public class AlreadyLiked extends RuntimeException{

}
