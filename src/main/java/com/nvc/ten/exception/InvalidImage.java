package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST,reason="Invalid image, there probably was no image data attached to packet")
public class InvalidImage extends RuntimeException{

}