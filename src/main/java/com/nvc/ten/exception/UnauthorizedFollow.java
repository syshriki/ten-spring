package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.UNAUTHORIZED,reason="Not authorized to follow user")
public class UnauthorizedFollow extends RuntimeException{

}
