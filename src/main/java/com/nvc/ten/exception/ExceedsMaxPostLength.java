package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST,reason="Post must have less than 500 characters")
public class ExceedsMaxPostLength extends RuntimeException{

}
