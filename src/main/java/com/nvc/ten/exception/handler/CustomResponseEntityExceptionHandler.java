package com.nvc.ten.exception.handler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nvc.ten.models.json.TenException;


@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends
		ResponseEntityExceptionHandler {

	@ExceptionHandler(MultipartException.class)
	@ResponseBody
	public  ResponseEntity<TenException> processValidationError(MultipartException ex){
		TenException te = new TenException(
				org.springframework.http.HttpStatus.PAYLOAD_TOO_LARGE.value(),
				org.springframework.http.HttpStatus.PAYLOAD_TOO_LARGE.name(),
				"Image must be less than 5mb");
		return new ResponseEntity<TenException>(te,org.springframework.http.HttpStatus.PAYLOAD_TOO_LARGE);
	}
}