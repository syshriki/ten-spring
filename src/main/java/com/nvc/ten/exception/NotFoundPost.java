package com.nvc.ten.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason="Could not find requested post, may have been deleted")
public class NotFoundPost extends RuntimeException{
}
