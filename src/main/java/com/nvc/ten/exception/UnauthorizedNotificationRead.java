package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.UNAUTHORIZED,reason="Cannot read notification")
public class UnauthorizedNotificationRead extends RuntimeException{

}
