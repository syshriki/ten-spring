package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST,reason="Cannot unblock since user is not blocked.")
public class UserNotBlocked extends RuntimeException{

}