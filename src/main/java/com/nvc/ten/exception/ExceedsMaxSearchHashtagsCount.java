package com.nvc.ten.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason="Max hashtag count exceeded ")
public class ExceedsMaxSearchHashtagsCount extends RuntimeException {

}
