package com.nvc.ten.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE,reason="Image needs to be a png,jpg,jpeg or gif")
public class BadImageFormat extends RuntimeException{

}
