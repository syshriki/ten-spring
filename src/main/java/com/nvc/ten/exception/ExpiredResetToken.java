package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.FORBIDDEN,reason="Token is expired")
public class ExpiredResetToken extends RuntimeException {

}
