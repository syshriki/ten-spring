package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.FORBIDDEN,reason="Please wait until all active posts expire.")
public class MustWaitUntilActivePostsExpire extends RuntimeException{

}
