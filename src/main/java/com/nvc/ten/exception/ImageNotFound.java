package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.NOT_FOUND,reason="No image found")
public class ImageNotFound extends RuntimeException{

}