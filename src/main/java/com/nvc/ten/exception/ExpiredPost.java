package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.FORBIDDEN,reason="Post is expired")
public class ExpiredPost extends RuntimeException {

}
