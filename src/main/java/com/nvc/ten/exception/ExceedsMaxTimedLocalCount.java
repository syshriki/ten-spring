package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST,reason="Exceeds max timed count")
public class ExceedsMaxTimedLocalCount extends RuntimeException{

}
