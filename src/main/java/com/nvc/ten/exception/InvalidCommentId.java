package com.nvc.ten.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason="Invalid comment id")
public class InvalidCommentId extends RuntimeException{

}
