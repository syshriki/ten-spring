package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.UNAUTHORIZED,reason="Cannot view comments")
public class UnauthorizedCommentRetrieval extends RuntimeException{

}
