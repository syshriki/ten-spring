package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST)
public class InvalidCredential extends RuntimeException{
public InvalidCredential(String message) {
	super(message);
}

}
