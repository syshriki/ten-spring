package com.nvc.ten.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST,reason="Cannot follow blocked user")
public class UserBlocked extends RuntimeException{

}