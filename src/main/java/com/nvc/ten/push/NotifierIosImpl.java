package com.nvc.ten.push;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.EnhancedApnsNotification;
import com.nvc.ten.dao.DeviceTokenDao;
import com.nvc.ten.dao.presistent.NotificationMysqlDao;
import com.nvc.ten.models.json.Notification;
import com.nvc.ten.models.json.Notification.NotificationType;
import com.nvc.ten.models.presistent.DeviceTokenTo;
import com.nvc.ten.models.presistent.NotificationTo;
import com.nvc.ten.utility.ObjectTransformUtility;

@Component
@Qualifier("ios")
public class NotifierIosImpl implements INotifier {

	@Autowired
	ApnsService apnsService;

	@Autowired
	ObjectTransformUtility objectTransformUtility;

	@Autowired
	DeviceTokenDao deviceTokenDao;

	@Autowired
	NotificationMysqlDao notificationDao;
	

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	public boolean isUserInactive(int idUser) {
		DeviceTokenTo deviceToken = deviceTokenDao.selectToken(idUser,"ios");
		// This condition should never be true since every user should have a
		// token, but just incase
		if (deviceToken == null)
			return false;
		Map<String, Date> inactiveDevices = apnsService.getInactiveDevices();
		for (String token : inactiveDevices.keySet()) {
			// Device is inactive so push notification should be sent
			if (token.equals(deviceToken.getToken()))
				return true;
		}
		// User token was not found in inactive token list, so user must be
		// active
		return false;
	}

	public void sendPushNotification(EnhancedApnsNotification notification){
		try{
			apnsService.push(notification);
		} catch(com.notnoop.exceptions.NetworkIOException e){
			System.out.println("certificate is fucked");
		}
	}
	public String buildPayload(Notification notification, int idUser) {
		
		String message = notification.getMessage();
		if (notification.getType()== NotificationType.LIKE){
			message = notification.getUser().getUsername() + " timed your post";
		} else if(notification.getType()== NotificationType.COMMENT){
			message = notification.getUser().getUsername() + " commented: \"" + notification.getMessage() +"\"";
		} else if(notification.getType()== NotificationType.REPOST){
			message = notification.getUser().getUsername() + " reposted your post";
		} else if(notification.getType() == NotificationType.MENTIONCOMMENT){
			message = notification.getUser().getUsername() + " mentioned you in a comment";
		}else if(notification.getType() == NotificationType.MENTIONPOST){
			message = notification.getUser().getUsername() + " mentioned you in a post";
		}
		
		int notificationCount = notificationDao.countUnreadNotificationByIdUser(idUser);
		
		return APNS.newPayload()
				.badge(notificationCount)
				.alertTitle("Ten")
				.alertAction("newnotif")
				.alertBody(message).sound("default")
				.instantDeliveryOrSilentNotification()
				.customField("notification", notification)
				.build();
	}

	public String getUserToken(int idUser) {
		DeviceTokenTo token = deviceTokenDao.selectToken(idUser,"ios");
		return token != null ? token.getToken() : null;
	}

	@Override
	public void repostNotification(int idAffectingUser, int idAffectedUser,
			int idPost) {
		String token = this.getUserToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setNotificationType(Notification.NotificationType.REPOST
				.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);

		sendPushNotification(pushNotification);

	}

	@Override
	public void likeNotification(int idAffectingUser, int idAffectedUser,
			int idPost, int idLike) {
		String token = this.getUserToken(idAffectedUser);
		// User is active so we don't need to send a push notification
		if (token == null)
			return;

		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setIdLike(idLike);
		notificationTo.setNotificationType(Notification.NotificationType.LIKE
				.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);

		sendPushNotification(pushNotification);
	}

	@Override
	public void commentNotification(int idAffectingUser, int idAffectedUser,
			int idPost, int idComment) {
		String token = this.getUserToken(idAffectedUser);
		// User is active so we don't need to send a push notification
		if (token == null)
			return;

		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setIdComment(idComment);
		notificationTo
				.setNotificationType(Notification.NotificationType.COMMENT
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);
		
		sendPushNotification(pushNotification);
	}

	@Override
	public void mentionCommentNotification(int idAffectingUser,
			int idAffectedUser, int idPost, int idComment) {
		String token = this.getUserToken(idAffectedUser);
		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setIdComment(idComment);
		notificationTo
				.setNotificationType(Notification.NotificationType.MENTIONCOMMENT
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);

		sendPushNotification(pushNotification);
	}

	@Override
	public void mentionPostNotification(int idAffectingUser,
			int idAffectedUser, int idPost) {
		String token = this.getUserToken(idAffectedUser);
		// User is active so we don't need to send a push notification
		if (token == null)
			return;

		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo
				.setNotificationType(Notification.NotificationType.MENTIONPOST
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);

		sendPushNotification(pushNotification);
	}

	@Override
	public void followNotification(int idAffectingUser, int idAffectedUser) {
		String token = this.getUserToken(idAffectedUser);
		// User is active so we don't need to send a push notification
		if (token == null)
			return;

		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setNotificationType(Notification.NotificationType.FOLLOW
				.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);

		sendPushNotification(pushNotification);
	}

	@Override
	public void followingNotification(int idAffectingUser, int idAffectedUser) {
		String token = this.getUserToken(idAffectedUser);
		// User is active so we don't need to send a push notification
		if (token == null)
			return;

		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo
				.setNotificationType(Notification.NotificationType.FOLLOWING
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);

		sendPushNotification(pushNotification);
	}

	@Override
	public void friendRequestNotification(int idAffectingUser,
			int idAffectedUser) {
		String token = this.getUserToken(idAffectedUser);
		// User is active so we don't need to send a push notification
		if (token == null)
			return;

		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo
				.setNotificationType(Notification.NotificationType.FRIENDREQUEST
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);

		String payload = buildPayload(notification,idAffectedUser);

		int now = (int) (new Date().getTime() / 1000);

		EnhancedApnsNotification pushNotification = new EnhancedApnsNotification(
				EnhancedApnsNotification.INCREMENT_ID() /* Next ID */,
				now + 60 * 60 /* Expire in one hour */, token /* Device Token */,
				payload);

		sendPushNotification(pushNotification);
	}

}
