package com.nvc.ten.push;
/*
 * Every notification type needs to be in here so the proper push
 * notification can be created
 */

public interface INotifier {
	public void likeNotification(int idAffectingUser, int idAffectedUser, int idPost, int idLike);
	public void repostNotification(int idAffectingUser, int idAffectedUser, int idPost);
	public void commentNotification(int idAffectingUser, int idAffectedUser, int idPost, int idComment);
	public void mentionCommentNotification(int idAffectingUser, int idAffectedUser, int idPost, int idComment);
	public void mentionPostNotification(int idAffectingUser, int idAffectedUser, int idPost);
	//public void postExpiredNotification();
	public void followNotification(int idAffectingUser, int idAffectedUser);
	public void followingNotification(int idAffectingUser, int idAffectedUser);
	public void friendRequestNotification(int idAffectingUser, int idAffectedUser);
}
