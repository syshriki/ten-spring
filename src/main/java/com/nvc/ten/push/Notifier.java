package com.nvc.ten.push;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("mothership")
public class Notifier implements INotifier{
	@Autowired
	List<INotifier> notifiers;
	
	@Override
	public void likeNotification(int idAffectingUser, int idAffectedUser,
			int idPost, int idLike) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.likeNotification(idAffectingUser, idAffectedUser, idPost, idLike);
		}
		
	}

	@Override
	public void repostNotification(int idAffectingUser, int idAffectedUser,
			int idPost) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.repostNotification(idAffectingUser, idAffectedUser, idPost);
		}
		
	}

	@Override
	public void commentNotification(int idAffectingUser, int idAffectedUser,
			int idPost, int idComment) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.commentNotification(idAffectingUser, idAffectedUser, idPost, idComment);
		}
		
	}

	@Override
	public void mentionCommentNotification(int idAffectingUser,
			int idAffectedUser, int idPost, int idComment) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.mentionCommentNotification(idAffectingUser, idAffectedUser, idPost, idComment);
		}
		
	}

	@Override
	public void mentionPostNotification(int idAffectingUser,
			int idAffectedUser, int idPost) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.mentionPostNotification(idAffectingUser, idAffectedUser, idPost);
		}
		
	}

	@Override
	public void followNotification(int idAffectingUser, int idAffectedUser) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.followNotification(idAffectingUser, idAffectedUser);
		}
		
	}

	@Override
	public void followingNotification(int idAffectingUser, int idAffectedUser) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.followingNotification(idAffectingUser, idAffectedUser);
		}
	}

	@Override
	public void friendRequestNotification(int idAffectingUser,
			int idAffectedUser) {
		for(INotifier n : notifiers){
			if(n instanceof Notifier){
				continue;
			}
			n.friendRequestNotification(idAffectingUser, idAffectedUser);
		}
		
	}
}
