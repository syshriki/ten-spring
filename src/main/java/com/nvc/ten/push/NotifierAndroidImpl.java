package com.nvc.ten.push;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.dao.DeviceTokenDao;
import com.nvc.ten.dao.presistent.NotificationMysqlDao;
import com.nvc.ten.models.json.Notification;
import com.nvc.ten.models.json.Notification.NotificationType;
import com.nvc.ten.models.presistent.DeviceTokenTo;
import com.nvc.ten.models.presistent.NotificationTo;
import com.nvc.ten.utility.ObjectTransformUtility;

@Component
@Qualifier("android")
public class NotifierAndroidImpl implements INotifier {
	@Value("${gcm.authorization}")
	private String authorizationKey;
	@Value("${gcm.api}")
	private String gcmApi;
	
	@Autowired
	private ObjectTransformUtility objectTransformUtility;
	
	@Autowired
	private DeviceTokenDao deviceTokenDao;
	
	@Autowired
	private NotificationMysqlDao notificationDao;
	
	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@Override
	public void likeNotification(int idAffectingUser, int idAffectedUser,
			int idPost, int idLike) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setIdLike(idLike);
		notificationTo.setNotificationType(Notification.NotificationType.LIKE
				.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		
		send(notification,token);
	}

	@Override
	public void repostNotification(int idAffectingUser, int idAffectedUser,
			int idPost) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setNotificationType(Notification.NotificationType.REPOST
				.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		
		send(notification,token);
	}

	@Override
	public void commentNotification(int idAffectingUser, int idAffectedUser,
			int idPost, int idComment) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setIdComment(idComment);
		notificationTo
				.setNotificationType(Notification.NotificationType.COMMENT
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		
		send(notification,token);
	}

	@Override
	public void mentionCommentNotification(int idAffectingUser,
			int idAffectedUser, int idPost, int idComment) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setIdComment(idComment);
		notificationTo
				.setNotificationType(Notification.NotificationType.MENTIONCOMMENT
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		
		send(notification,token);
	}

	@Override
	public void mentionPostNotification(int idAffectingUser,
			int idAffectedUser, int idPost) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setId(idPost);
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo
				.setNotificationType(Notification.NotificationType.MENTIONPOST
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		send(notification,token);
	}

	@Override
	public void followNotification(int idAffectingUser, int idAffectedUser) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo.setNotificationType(Notification.NotificationType.FOLLOW
				.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		send(notification,token);
	}

	@Override
	public void followingNotification(int idAffectingUser, int idAffectedUser) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo
				.setNotificationType(Notification.NotificationType.FOLLOWING
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		send(notification,token);
	}

	@Override
	public void friendRequestNotification(int idAffectingUser,
			int idAffectedUser) {
		String token = this.getAndroidToken(idAffectedUser);

		// User is active so we don't need to send a push notification
		if (token == null)
			return;
		
		NotificationTo notificationTo = new NotificationTo();
		notificationTo.setIdAffected(idAffectedUser);
		notificationTo.setIdAffecting(idAffectingUser);
		notificationTo
				.setNotificationType(Notification.NotificationType.FRIENDREQUEST
						.toString());
		Notification notification = objectTransformUtility
				.notificationToToNotification(notificationTo);
		send(notification,token);
	}

	private void send(Notification notification, String userToken){
		String message = notification.getMessage();
		if (notification.getType()== NotificationType.LIKE){
			message = notification.getUser().getUsername() + " timed your post";
		} else if(notification.getType()== NotificationType.COMMENT){
			message = notification.getUser().getUsername() + " commented: \"" + notification.getMessage() +"\"";
		} else if(notification.getType()== NotificationType.REPOST){
			message = notification.getUser().getUsername() + " reposted your post";
		} else if(notification.getType() == NotificationType.MENTIONCOMMENT){
			message = notification.getUser().getUsername() + " mentioned you in a comment";
		}else if(notification.getType() == NotificationType.MENTIONPOST){
			message = notification.getUser().getUsername() + " mentioned you in a post";
		}
		notification.setMessage(message);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "key="+authorizationKey);

		HashMap<String,Object> body = new HashMap<>();
	    body.put("data", notification);
	    body.put("to", userToken);
	    
		try {
			String bodyJson = jacksonObjectMapper.writeValueAsString(body);
		    HttpEntity<String> entity = new HttpEntity<String>(bodyJson,headers);
		    RestTemplate restTemplate = new RestTemplate();
		    restTemplate.postForObject(gcmApi, entity, String.class);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
	public String getAndroidToken(int idUser) {
		DeviceTokenTo token = deviceTokenDao.selectToken(idUser, "android");
		return token != null ? token.getToken() : null;
	}

}
