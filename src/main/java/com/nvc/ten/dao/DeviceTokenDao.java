package com.nvc.ten.dao;

import com.nvc.ten.models.presistent.DeviceTokenTo;

public interface DeviceTokenDao {

	public int updateToken(String token, int idUser, String type);

	public int deleteToken(int idUser, String type);

	public int insertToken(String token, int idUser, String type);

	public DeviceTokenTo selectToken(String username, String type);

	public DeviceTokenTo selectToken(int idUser, String type);
}