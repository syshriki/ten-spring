package com.nvc.ten.dao;

import java.util.List;

import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

public interface SearchDao {

	public List<UserTo> selectUsersByLikeUsername(int idUser, String username,
			int offset, int count);

	public List<PostTo> selectPostsByLikeHashtag(int idUser, String hashtag,
			int offset, int count);

}