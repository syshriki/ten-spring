package com.nvc.ten.dao;

import com.nvc.ten.models.presistent.UserTo;

public interface LoginDao {
	public UserTo selectUser(int idUser);
}
