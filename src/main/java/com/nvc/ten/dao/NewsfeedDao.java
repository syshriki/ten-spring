package com.nvc.ten.dao;

import java.util.List;

import com.nvc.ten.models.presistent.PostTo;

public interface NewsfeedDao {
	public List<PostTo> selectNewsfeedPostsByIdUser(int idUser, int offset,int count);
}
