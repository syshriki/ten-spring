package com.nvc.ten.dao;

import java.util.Date;
import java.util.List;

import com.nvc.ten.models.presistent.BlockTo;
import com.nvc.ten.models.presistent.PasswordResetToken;
import com.nvc.ten.models.presistent.UserTo;

public interface UserActionDao {

	public int updateFollowerApprove(int idRequestee, int idRecipient);

	public int updateFollowerReject(int idRequestee, int idRecipient);

	public int updateFollowerPending(int idRequestee, int idRecipient);

	public int insertBlock(int idBlocked, int idBlocker);

	public int deleteBlock(int idBlocked, int idBlocker);

	public int deleteFollow(int idRequestee, int idRecipient);

	public BlockTo selectBlockByIdBlockedAndIdBlocker(int idBlocked,
			int idBlocker);

	public List<UserTo> selectFollowerPending(int idUser, int offset, int count);

	public int insertFollowApproved(int idRequestee, int idRecipient);

	public int insertFollowPending(int idRequestee, int idRecipient);

	public int deleteFollowPending(int idRequestee, int idRecipient);

	public int insertPasswordResetToken(String token, int idUser, Date expiry);

	public PasswordResetToken selectPasswordResetTokenByToken(String token);

	public int updatePasswordresetByToken(String token);

}