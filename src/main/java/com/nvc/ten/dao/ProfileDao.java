package com.nvc.ten.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

public interface ProfileDao {

	public Integer countByEmail(String email);

	public Integer countByUsername(String username);

	public PostTo selectPostMostLikedByIdUser(int idUser);

	public PostTo selectPostMostLikedByUsername(String username);

	public List<PostTo> selectPostAllByUsername(String username, int offset,
			int count);

	public List<PostTo> selectPostAllByIdUser(int idUser, int offset, int count);

	public List<PostTo> selectPostActiveByIdUser(int idUser, int offset,
			int count);

	public List<PostTo> selectPostActiveByUsername(String username, int offset,
			int count);

	public List<UserTo> selectFollowingLikeUsername(int idUser,
			String username, int offset, int count);

	public List<UserTo> selectFollowersLikeUsername(int idUser,
			String username, int offset, int count);

	public List<UserTo> selectFollowingByIdUser(int idUser, int offset,
			int count);

	public List<UserTo> selectFollowersByIdUser(int idUser, int offset,
			int count);

	public Integer countPostsAllByIdUser(int idUser);

	public Integer countFollowersByIdUser(int idUser);

	public Integer countFollowingByIdUser(int idUser);

	public Integer countPostsActiveByIdUser(int idUser);

	public Integer countLikesOnIdUser(int idUser);

	public Integer countCommentsOnIdUser(int idUser);

	public int updateUserProfile(String username, String description,
			String email, String website, int isPrivate, int idUser);

	public int updateUserPassword(String password, int idUser);

	public int updateUserImage(String imageSrc, int idUser);

	public List<UserTo> selectLikersTopByIdUser(int idUser);

}