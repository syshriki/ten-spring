package com.nvc.ten.dao;

import java.util.Date;
import java.util.List;

import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.LikeTo;
import com.nvc.ten.models.presistent.PostHashtagTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

public interface PostDao {
	public int insertPost(String text, int idUser,int idUserPostRepost,int hasImage,Date expiration,Date datestamp);
	public Integer insertNotification(String notificationType, int idAffectingUser,int idAffectedUser, int idPost);
	public int insertImage(int idUserPost, String imageSrc);
	public int insertHashTag(String hash);
	public int insertLike(int idPost, int idUser);
	public int insertPostHashTag(int idUserPost, int idHashtags);
	public int callMentionPostInsert(int idMentioner, int idMentioned,int idUserpost);
	public int callMentionCommentInsert(int idMentioner, int idMentioned,int idUserpost, int idComment);
	public Integer selectIdUserByUsername(String username);
	public Integer selectIdHashtags(String hash);
	public PostTo selectPostByIdPost(int idPost);
	public List<PostTo> selectPostsByIdRepost(int idPost);
	public String selectImageByIdPost(int idPost);
	public Integer countLikeByUserForPost(int idUser, int idPost);
	public int updateExpiration(int idPost, Date expiration);
	public int insertComment(String text, int idPost, int idUser,Date datestamp);
	public CommentTo selectCommentByIdComment(int idComment);
	public int deleteComment(int idComment);
	public List<LikeTo> selectLikesByIdPost(int idPost);
	public List<CommentTo> selectCommentsByIdPost(int idPost, int offset,int count);
	public List<PostHashtagTo> selectPostHashtagsByIdPost(int idPost);
	public List<UserTo> selectLikersByIdPost(int idPost, int offset, int count);
	public int deleteLikesByIdPost(int idPost);
	public int deleteImagesByIdPost(int idPost);
	public int deleteCommentsByIdPost(int idPost);
	public int deleteNotificationsByIdPost(int idPost);
	public int deleteNotificationsByIdLike(int idLike);
	public int deleteNotificationsByIdComment(int idLike);
	public int deletePostHashtagsByIdPost(int idPost);
	public int deletePostByIdPost(int idPost);
}
