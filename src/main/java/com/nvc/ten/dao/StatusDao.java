package com.nvc.ten.dao;

import java.util.List;

import com.nvc.ten.models.presistent.NotificationTo;

public interface StatusDao {

	public List<NotificationTo> selectStatusByIdUser(int idUser, int offset,
			int count);

}