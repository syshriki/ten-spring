package com.nvc.ten.dao;

import com.nvc.ten.models.presistent.BlockTo;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.FollowTo;
import com.nvc.ten.models.presistent.ImageTo;
import com.nvc.ten.models.presistent.LikeTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

public interface UtilityDao {

	public FollowTo selectFollower(int idRequestee, int idRecipient);

	// TODO login has the same method
	public UserTo selectUserByIdUser(int idUser);

	public UserTo selectUserByEmail(String email);

	public UserTo selectUserByUsername(String username);

	public PostTo selectPostByIdPost(int idPost);

	public PostTo selectPostByRepostedAndIdUser(int idPost, int idUser);

	public ImageTo selectImageByIdPost(int idPost);

	public LikeTo selectLikeByIdPostAndIdUser(int idPost, int idUser);

	public LikeTo selectLikeByIdLike(int idLike);

	public CommentTo selectCommentByIdPostAndIdUser(int idPost, int idUser);

	public CommentTo selectCommentByIdComment(int idComment);

	public BlockTo selectBlockByIdBlockedAndIdBlocker(int idBlocked,
			int idBlocker);

}