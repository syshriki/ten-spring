package com.nvc.ten.dao;

import java.util.List;

import com.nvc.ten.models.presistent.NotificationTo;

public interface NotificationDao {
	public NotificationTo selectNotificationByIdNotification(int idNotification);
	public Integer insertNotification(String notificationType, int idPost, int idAffectingUser,int idAffectedUser);
	public Integer insertCommentNotification(String notificationType, int idPost, int idAffectingUser,int idAffectedUser, int idComment);
	public Integer insertLikeNotification(String notificationType, int idPost, int idAffectingUser,int idAffectedUser, int idLike);
	public Integer insertNotification(String notificationType, int idAffectingUser,int idAffectedUser);
	public Integer countUnreadNotificationByIdUser(int idUser);
	public Integer updateUnreadNotificationRead(int idNotification);
	public List<NotificationTo> selectUnreadNotificationByIdUser(int idUser, int offset, int count);
}
