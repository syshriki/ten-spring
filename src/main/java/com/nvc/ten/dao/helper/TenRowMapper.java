package com.nvc.ten.dao.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nvc.ten.models.presistent.BlockTo;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.FollowTo;
import com.nvc.ten.models.presistent.ImageTo;
import com.nvc.ten.models.presistent.LikeTo;
import com.nvc.ten.models.presistent.NotificationTo;
import com.nvc.ten.models.presistent.PasswordResetToken;
import com.nvc.ten.models.presistent.PostHashtagTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

public class TenRowMapper {

	public PostTo mapRowPost(ResultSet rs, int rowNum) throws SQLException {
		PostTo p = new PostTo();
		p.setDatestamp(rs.getTimestamp("datestamp"));
		p.setExpiration(rs.getTimestamp("expiration"));
		p.setIdPost(rs.getInt("iduserpost"));
		p.setIdRepost(rs.getInt("iduserpostrepost"));
		p.setIdUser(rs.getInt("iduser"));
		p.setText(rs.getString("text"));
		p.setHasImage(rs.getInt("hasimage"));
		return p;
	}

	public UserTo mapRowUser(ResultSet rs, int rowNum) throws SQLException {
		UserTo u = new UserTo();
		u.setIdUser(rs.getInt("iduser"));
		u.setUsername(rs.getString("username"));
		u.setPassword(rs.getString("password"));
		u.setDatestamp(rs.getTimestamp("datestamp"));
		u.setDescription(rs.getString("description"));
		u.setEmail(rs.getString("email"));
		u.setImageSrc(rs.getString("imagesrc"));
		u.setWebsite(rs.getString("website"));
		u.setPrivate(rs.getInt("isprivate") == 1);
		return u;
	}

	public FollowTo mapRowFollower(ResultSet rs, int rowNum)
			throws SQLException {
		FollowTo f = new FollowTo();
		f.setDatestamp(rs.getTimestamp("datestamp"));
		f.setApproved(rs.getInt("approved"));
		f.setIdFollowers(rs.getInt("idfollowers"));
		f.setIdRecipient(rs.getInt("idrecipient"));
		f.setIdRequestee(rs.getInt("idrequestee"));
		return f;
	}

	public CommentTo mapRowComment(ResultSet rs, int rowNum)
			throws SQLException {
		CommentTo c = new CommentTo();
		c.setDatestamp(rs.getTimestamp("datestamp"));
		c.setIdComment(rs.getInt("idusercomment"));
		c.setIdPost(rs.getInt("iduserpost"));
		c.setText(rs.getString("text"));
		c.setIdUser(rs.getInt("iduser"));
		return c;
	}

	public LikeTo mapRowLike(ResultSet rs, int rowNum) throws SQLException {
		LikeTo l = new LikeTo();
		l.setDatestamp(rs.getTimestamp("datestamp"));
		l.setIdUser(rs.getInt("iduser"));
		l.setIdPost(rs.getInt("iduserpost"));
		l.setIdLike(rs.getInt("iduserpostlikes"));
		return l;
	}

	public ImageTo mapRowImage(ResultSet rs, int rowNum) throws SQLException {
		ImageTo i = new ImageTo();
		i.setIdImage(rs.getInt("iduserpostimage"));
		i.setIdPost(rs.getInt("iduserpost"));
		i.setImageSrc(rs.getString("imagesrc"));
		i.setDatestamp(rs.getTimestamp("datestamp"));
		return i;
	}

	public PostHashtagTo mapRowPostHashtag(ResultSet rs, int rowNum)
			throws SQLException {
		PostHashtagTo h = new PostHashtagTo();
		h.setIdPostHashtag(rs.getInt("iduserposthashtag"));
		h.setIdHashtag(rs.getInt("idhashtag"));
		h.setIdPost(rs.getInt("iduserpost"));
		h.setDatestamp(rs.getTimestamp("datestamp"));
		return h;
	}

	public BlockTo mapRowBlock(ResultSet rs, int rowNum) throws SQLException {
		BlockTo b = new BlockTo();
		b.setIdBlock(rs.getInt("iduserblocks"));
		b.setIdBlocked(rs.getInt("iduserblocked"));
		b.setIdBlocker(rs.getInt("iduserblocker"));
		b.setDatestamp(rs.getTimestamp("datestamp"));
		return b;
	}
	
	public NotificationTo mapRowNotification(ResultSet rs, int rowNum) throws SQLException {
		NotificationTo n = new NotificationTo();
		n.setId(rs.getInt("id"));
		n.setIdAffected(rs.getInt("affecteduser"));
		n.setIdAffecting(rs.getInt("affectinguser"));
		n.setIdNotification(rs.getInt("idunreadnotifications"));
		n.setNotificationType(rs.getString("notificationtype"));
		n.setDatestamp(rs.getTimestamp("datestamp"));
		n.setIdNotification(rs.getInt("idunreadnotifications"));
		n.setIdComment(rs.getInt("idcomment"));
		n.setIdLike(rs.getInt("idlike"));
		return n;
	}
	
	public PasswordResetToken mapRowPasswordResetToken(ResultSet rs, int rowNum) throws SQLException {
		PasswordResetToken p = new PasswordResetToken();
		p.setExpiryDate(rs.getTimestamp("expirydate"));
		p.setId(rs.getInt("idpasswordresettoken"));
		p.setIsUsed(rs.getInt("used")==1);
		p.setToken(rs.getString("token"));
		p.setIdUser(rs.getInt("iduser"));
		return p;
	}
}
