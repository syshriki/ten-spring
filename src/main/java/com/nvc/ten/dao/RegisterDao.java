package com.nvc.ten.dao;

public interface RegisterDao {

	public int insertUser(String username, String password, String description,
			String email, String imageSrc, String website, int isprivate);

	public Integer countByEmail(String email);

	public Integer countByUsername(String username);

}