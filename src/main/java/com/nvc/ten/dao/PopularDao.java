package com.nvc.ten.dao;

import java.util.List;

import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

public interface PopularDao {
	public List<UserTo> selectTopTimerLocalByIdPost(int idUser, int offset, int count);
	public List<UserTo> selectTopTimerGlobalByIdPost(int idUser, int offset, int count);
	public List<PostTo> selectTopTimedLocalByIdPost(int idUser, int offset,int count);
	public List<PostTo> selectTopTimedGlobalByIdPost(int idUser, int offset,int count);
}
