package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.ProfileDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

@Repository
public class ProfileMysqlDao implements ProfileDao {
	// Best time
	private final String SELECT_POST_MOST_LIKED_BY_IDUSER = "SELECT userpost.*,COUNT(distinct iduserpostlikes) as timeweight FROM userpost LEFT JOIN userpostlikes as likecount ON likecount.iduserpost = userpost.iduserpost WHERE userpost.iduser = ? GROUP BY userpost.iduserpost ORDER BY timeweight DESC LIMIT 1";
	private final String SELECT_POST_MOST_LIKED_BY_USERNAME = "SELECT userpost.*,COUNT(distinct iduserpostlikes) as timeweight FROM userpost INNER JOIN user AS u ON u.iduser=userpost.iduser LEFT JOIN userpostlikes AS likecount ON likecount.iduserpost = userpost.iduserpost WHERE u.username = ? GROUP BY userpost.iduserpost ORDER BY timeweight DESC LIMIT 1";
	private final String SELECT_FOLLOWING_BY_IDUSER = "SELECT user.* FROM user WHERE EXISTS (SELECT * FROM followers WHERE user.iduser = followers.idrecipient AND followers.idrequestee = ? AND followers.approved = 1) ORDER BY user.username LIMIT ?,?";
	private final String SELECT_FOLLOWERS_BY_IDUSER = "SELECT user.* FROM user WHERE EXISTS (SELECT * FROM followers WHERE user.iduser = followers.idrequestee AND followers.idrecipient = ? AND followers.approved = 1) ORDER BY user.username LIMIT ?,?";
	private final String SELECT_POSTS_ALL_BY_IDUSER = "SELECT userpost.* FROM userpost WHERE userpost.iduser = ? ORDER BY userpost.expiration DESC LIMIT ?,?";
	private final String SELECT_POSTS_ALL_BY_USERNAME = "SELECT userpost.* FROM userpost INNER JOIN user AS u ON u.iduser=userpost.iduser WHERE u.username = ? ORDER BY userpost.expiration DESC LIMIT ?,?";
	private final String SELECT_POSTS_ACTIVE_BY_IDUSER = "SELECT userpost.* FROM userpost WHERE userpost.iduser = ? AND userpost.expiration >= NOW() ORDER BY userpost.expiration DESC LIMIT ?,?";
	private final String SELECT_POSTS_ACTIVE_BY_USERNAME = "SELECT userpost.* FROM userpost INNER JOIN user AS u ON u.iduser=userpost.iduser WHERE u.username = ? AND userpost.expiration >= NOW() ORDER BY userpost.expiration DESC LIMIT ?,?";
	private final String SELECT_LIKERS_TOP_BY_IDUSER = "SELECT user.*,COUNT(*) as count FROM userpostlikes LEFT JOIN user ON userpostlikes.iduser = user.iduser WHERE userpostlikes.iduserpost IN (SELECT iduserpost FROM userpost WHERE userpost.iduser = ?) AND user.iduser != ? GROUP BY iduser ORDER BY count DESC LIMIT 3";
	private final String SELECT_FOLLOWING_LIKE_USERNAME = "(SELECT u.* FROM followers AS f INNER JOIN user AS u ON f.idrecipient=u.iduser WHERE f.idrequestee = ? AND f.approved = 1 AND ? NOT IN(SELECT b.iduserblocked FROM userblocks AS b WHERE b.iduserblocker=u.iduser) AND u.username LIKE ? LIMIT ?,?)";
	private final String SELECT_FOLLOWERS_LIKE_USERNAME = "(SELECT u.* FROM followers AS f INNER JOIN user AS u ON f.idrequestee=u.iduser WHERE f.idrecipient = ? AND f.approved = 1 AND ? NOT IN(SELECT b.iduserblocked FROM userblocks AS b WHERE b.iduserblocker=u.iduser) AND u.username LIKE ? LIMIT ?,?)";

	private final String COUNT_BY_EMAIL = "SELECT COUNT(u.iduser) FROM user AS u WHERE u.email = ?";
	private final String COUNT_BY_USERNAME = "SELECT COUNT(u.iduser) FROM user AS u WHERE u.username = ?";
	private final String COUNT_POSTS_BY_IDUSER = "(SELECT COUNT(userpost.iduserpost) FROM userpost,user WHERE user.iduser = ? AND userpost.iduser = user.iduser)";
	private final String COUNT_FOLLOWERS_BY_IDUSER = "(SELECT COUNT(followers.idfollowers) FROM followers,user WHERE user.iduser = followers.idrequestee AND followers.idrecipient = ? AND followers.approved = 1)";
	private final String COUNT_FOLLOWING_BY_IDUSER = "(SELECT COUNT(followers.idfollowers) FROM followers,user WHERE user.iduser = followers.idrecipient AND followers.idrequestee = ? AND followers.approved = 1)";
	private final String COUNT_POSTS_ACTIVE_BY_IDUSER = "SELECT COUNT(*) FROM userpost WHERE userpost.expiration >= NOW() AND userpost.iduser = ?";
	//Likes user has recieved on their post
	private final String COUNT_LIKES_ON_IDUSER = "SELECT COUNT(*) as likecount FROM userpostlikes WHERE (userpostlikes.iduserpost) IN (SELECT userpost.iduserpost as posts FROM userpost WHERE userpost.iduser = ?) GROUP BY userpostlikes.iduserpost ORDER BY likecount DESC LIMIT 1";
	private final String COUNT_COMMENTS_ON_IDUSER = "SELECT COUNT(*) as commentcount FROM usercomment WHERE (usercomment.iduserpost) IN (SELECT userpost.iduserpost as posts FROM userpost where userpost.iduser = ?) GROUP BY usercomment.iduserpost ORDER BY commentcount DESC LIMIT 1";

	private final String UPDATE_USER_PROFILE = "UPDATE user SET username=? , description=? , email=?, website=?, isprivate=? WHERE iduser=?";
	private final String UPDATE_USER_PASSWORD = "UPDATE user SET password=? WHERE iduser=?";
	private final String UPDATE_USER_IMAGE = "UPDATE user SET imagesrc=? WHERE iduser=?";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	private TenRowMapper tenRowMapper;
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countByEmail(java.lang.String)
	 */
	@Override
	public Integer countByEmail(String email) {
		Integer idUser = (Integer) jdbcTemplate.queryForObject(COUNT_BY_EMAIL,new Object[] {email},Integer.class);
		return idUser;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countByUsername(java.lang.String)
	 */
	@Override
	public Integer countByUsername(String username) {
		Integer idUser = (Integer) jdbcTemplate.queryForObject(COUNT_BY_USERNAME,new Object[] {username},Integer.class);
		return idUser;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectPostMostLikedByIdUser(int)
	 */
	@Override
	public PostTo selectPostMostLikedByIdUser(int idUser) {
		try {
			PostTo postTo = jdbcTemplate.queryForObject(

			SELECT_POST_MOST_LIKED_BY_IDUSER, new Object[] { idUser },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectPostMostLikedByUsername(java.lang.String)
	 */
	@Override
	public PostTo selectPostMostLikedByUsername(String username) {
		try {
			PostTo postTo = jdbcTemplate.queryForObject(

			SELECT_POST_MOST_LIKED_BY_USERNAME, new Object[] { username },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectPostAllByUsername(java.lang.String, int, int)
	 */
	@Override
	public List<PostTo> selectPostAllByUsername(String username, int offset, int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(

					SELECT_POSTS_ALL_BY_USERNAME, new Object[] { username,offset,count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectPostAllByIdUser(int, int, int)
	 */
	@Override
	public List<PostTo> selectPostAllByIdUser(int idUser, int offset, int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(

			SELECT_POSTS_ALL_BY_IDUSER, new Object[] { idUser,offset,count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectPostActiveByIdUser(int, int, int)
	 */
	@Override
	public List<PostTo> selectPostActiveByIdUser(int idUser, int offset,
			int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(

			SELECT_POSTS_ACTIVE_BY_IDUSER, new Object[] { idUser,offset,count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectPostActiveByUsername(java.lang.String, int, int)
	 */
	@Override
	public List<PostTo> selectPostActiveByUsername(String username, int offset,
			int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(

			SELECT_POSTS_ACTIVE_BY_USERNAME, new Object[] { username,offset,count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectFollowingLikeUsername(int, java.lang.String, int, int)
	 */
	@Override
	public List<UserTo> selectFollowingLikeUsername(int idUser, String username, int offset,
			int count) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_FOLLOWING_LIKE_USERNAME, new Object[] { idUser,idUser,"%"+username+"%", offset,
							count }, new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectFollowersLikeUsername(int, java.lang.String, int, int)
	 */
	@Override
	public List<UserTo> selectFollowersLikeUsername(int idUser, String username, int offset,
			int count) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_FOLLOWERS_LIKE_USERNAME, new Object[] { idUser,idUser,"%"+username+"%", offset,
							count }, new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectFollowingByIdUser(int, int, int)
	 */
	@Override
	public List<UserTo> selectFollowingByIdUser(int idUser, int offset,
			int count) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_FOLLOWING_BY_IDUSER, new Object[] { idUser, offset,
							count }, new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectFollowersByIdUser(int, int, int)
	 */
	@Override
	public List<UserTo> selectFollowersByIdUser(int idUser, int offset,
			int count) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_FOLLOWERS_BY_IDUSER, new Object[] { idUser, offset,
							count }, new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countPostsAllByIdUser(int)
	 */
	@Override
	public Integer countPostsAllByIdUser(int idUser) {
		try {
			Integer count = (Integer) jdbcTemplate.queryForObject(
					COUNT_POSTS_BY_IDUSER, new Object[] { idUser },
					new int[] { Types.INTEGER }, Integer.class);
			return count;
		} catch (EmptyResultDataAccessException ex) {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countFollowersByIdUser(int)
	 */
	@Override
	public Integer countFollowersByIdUser(int idUser) {
		try {
			Integer count = (Integer) jdbcTemplate.queryForObject(
					COUNT_FOLLOWERS_BY_IDUSER, new Object[] { idUser },
					new int[] { Types.INTEGER }, Integer.class);
			return count;
		} catch (EmptyResultDataAccessException ex) {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countFollowingByIdUser(int)
	 */
	@Override
	public Integer countFollowingByIdUser(int idUser) {
		try {
			Integer count = (Integer) jdbcTemplate.queryForObject(
					COUNT_FOLLOWING_BY_IDUSER, new Object[] { idUser },
					new int[] { Types.INTEGER }, Integer.class);
			return count;
		} catch (EmptyResultDataAccessException ex) {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countPostsActiveByIdUser(int)
	 */
	@Override
	public Integer countPostsActiveByIdUser(int idUser) {
		try {
			Integer count = (Integer) jdbcTemplate.queryForObject(
					COUNT_POSTS_ACTIVE_BY_IDUSER, new Object[] { idUser },
					new int[] { Types.INTEGER }, Integer.class);
			return count;
		} catch (EmptyResultDataAccessException ex) {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countLikesOnIdUser(int)
	 */
	@Override
	public Integer countLikesOnIdUser(int idUser) {
		try {
			Integer count = (Integer) jdbcTemplate.queryForObject(
					COUNT_LIKES_ON_IDUSER, new Object[] { idUser }, new int[] {
							Types.INTEGER}, Integer.class);
			return count;
		} catch (EmptyResultDataAccessException ex) {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#countCommentsOnIdUser(int)
	 */
	@Override
	public Integer countCommentsOnIdUser(int idUser) {
		try {
			Integer count = (Integer) jdbcTemplate.queryForObject(
					COUNT_COMMENTS_ON_IDUSER, new Object[] { idUser },
					new int[] { Types.INTEGER }, Integer.class);
			return count;
		} catch (EmptyResultDataAccessException ex) {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#updateUserProfile(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int)
	 */
	@Override
	public int updateUserProfile(String username, String description,
			String email, String website, int isPrivate,int idUser) {
		int row = jdbcTemplate.update(UPDATE_USER_PROFILE, new Object[] { username,
				 description, email, website,isPrivate, idUser }, new int[] {
				Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.TINYINT, Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#updateUserPassword(java.lang.String, int)
	 */
	@Override
	public int updateUserPassword(String password,int idUser) {
		int row = jdbcTemplate.update(UPDATE_USER_PASSWORD, new Object[] { password,idUser }, new int[] {
				Types.VARCHAR,Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#updateUserImage(java.lang.String, int)
	 */
	@Override
	public int updateUserImage(String imageSrc,int idUser) {
		int row = jdbcTemplate.update(UPDATE_USER_IMAGE, new Object[] { imageSrc,idUser }, new int[] {
				Types.VARCHAR,Types.INTEGER});
		return row;
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IProfileDao#selectLikersTopByIdUser(int)
	 */
	@Override
	public List<UserTo> selectLikersTopByIdUser(int idUser) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_LIKERS_TOP_BY_IDUSER, new Object[] { idUser,idUser },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

}
