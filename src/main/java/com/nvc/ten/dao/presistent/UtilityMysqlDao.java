package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.UtilityDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.BlockTo;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.FollowTo;
import com.nvc.ten.models.presistent.ImageTo;
import com.nvc.ten.models.presistent.LikeTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

@Repository
public class UtilityMysqlDao implements UtilityDao {
	private final String SELECT_FOLLOWER = "SELECT * FROM followers WHERE idrequestee = ? AND idrecipient = ? LIMIT 1";
	private final String SELECT_USER_BY_IDUSER = "SELECT * FROM user AS u WHERE u.iduser = ? LIMIT 1";
	private final String SELECT_USER_BY_EMAIL = "SELECT * FROM user AS u WHERE u.email = ? LIMIT 1";
	private final String SELECT_USER_BY_USERNAME = "SELECT * FROM user AS u WHERE u.username = ? LIMIT 1";
	private final String SELECT_POST_BY_IDPOST = "SELECT * FROM userpost WHERE iduserpost = ? LIMIT 1";
	private final String SELECT_IMAGE_BY_IDPOST = "SELECT * FROM userpostimage WHERE iduserpost = ? LIMIT 1";
	private final String SELECT_LIKE_BY_IDPOST_AND_IDUSER = "SELECT * FROM userpostlikes WHERE iduserpost = ? AND iduser = ? LIMIT 1";
	private final String SELECT_COMMENT_BY_IDPOST_AND_IDUSER = "SELECT * FROM usercomment WHERE iduserpost = ? AND iduser = ? LIMIT 1";
	private final String SELECT_COMMENT_BY_IDCOMMENT = "SELECT * FROM usercomment WHERE idusercomment = ?";
	private final String SELECT_BLOCK_BY_IDBLOCKED_AND_IDBLOCKER = "SELECT * FROM userblocks WHERE iduserblocked = ? AND iduserblocker = ? LIMIT 1";
	private final String SELECT_LIKE_BY_IDLIKE = "SELECT * FROM userpostlikes WHERE iduserpostlikes = ? LIMIT 1";
	private final String SELECT_POST_BY_REPOSTED_AND_USER = "SELECT * FROM userpost WHERE iduserpostrepost=? AND iduser=? LIMIT 1";
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	private TenRowMapper tenRowMapper;

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectFollower(int, int)
	 */
	@Override
	public FollowTo selectFollower(int idRequestee, int idRecipient) {
		try {
			FollowTo followTo = (FollowTo) jdbcTemplate.queryForObject(
					SELECT_FOLLOWER, new Object[] { idRequestee, idRecipient },
					new RowMapper<FollowTo>() {
						public FollowTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowFollower(rs,rowNum);
						}
					});
			return followTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	// TODO login has the same method
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectUserByIdUser(int)
	 */
	@Override
	public UserTo selectUserByIdUser(int idUser) {
		try {
			UserTo userTO = (UserTo) jdbcTemplate.queryForObject(
					SELECT_USER_BY_IDUSER, new Object[] { idUser },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTO;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectUserByEmail(java.lang.String)
	 */
	@Override
	public UserTo selectUserByEmail(String email) {
		try {
			UserTo userTO = (UserTo) jdbcTemplate.queryForObject(
					SELECT_USER_BY_EMAIL, new Object[] { email },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTO;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectUserByUsername(java.lang.String)
	 */
	@Override
	public UserTo selectUserByUsername(String username) {
		try {
			UserTo userTO = (UserTo) jdbcTemplate.queryForObject(
					SELECT_USER_BY_USERNAME, new Object[] { username },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTO;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectPostByIdPost(int)
	 */
	@Override
	public PostTo selectPostByIdPost(int idPost) {
		try {
			PostTo postTo = (PostTo) jdbcTemplate.queryForObject(
					SELECT_POST_BY_IDPOST, new Object[] { idPost },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectPostByRepostedAndIdUser(int, int)
	 */
	@Override
	public PostTo selectPostByRepostedAndIdUser(int idPost, int idUser) {
		try {
			PostTo postTo = (PostTo) jdbcTemplate.queryForObject(
					SELECT_POST_BY_REPOSTED_AND_USER, new Object[] { idPost,idUser },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectImageByIdPost(int)
	 */
	@Override
	public ImageTo selectImageByIdPost(int idPost) {
		try {
			ImageTo imageTo = (ImageTo) jdbcTemplate.queryForObject(
					SELECT_IMAGE_BY_IDPOST, new Object[] { idPost },
					new RowMapper<ImageTo>() {
						public ImageTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowImage(rs, rowNum);
						}
					});
			return imageTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectLikeByIdPostAndIdUser(int, int)
	 */
	@Override
	public LikeTo selectLikeByIdPostAndIdUser(int idPost,int idUser) {
		try {
			LikeTo likeTo = (LikeTo) jdbcTemplate.queryForObject(SELECT_LIKE_BY_IDPOST_AND_IDUSER,
					new Object[] { idPost,idUser }, new RowMapper<LikeTo>() {
						public LikeTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowLike(rs, rowNum);
						}
					});
			return likeTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectLikeByIdLike(int)
	 */
	@Override
	public LikeTo selectLikeByIdLike(int idLike) {
		try {
			LikeTo likeTo = (LikeTo) jdbcTemplate.queryForObject(SELECT_LIKE_BY_IDLIKE,
					new Object[] { idLike }, new RowMapper<LikeTo>() {
						public LikeTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowLike(rs, rowNum);
						}
					});
			return likeTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectCommentByIdPostAndIdUser(int, int)
	 */
	@Override
	public CommentTo selectCommentByIdPostAndIdUser(int idPost, int idUser) {
		try {
			CommentTo commentTo = (CommentTo) jdbcTemplate.queryForObject(
					SELECT_COMMENT_BY_IDPOST_AND_IDUSER, new Object[] { idPost,idUser },
					new RowMapper<CommentTo>() {
						public CommentTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowComment(rs, rowNum);
						}
					});
			return commentTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectCommentByIdComment(int)
	 */
	@Override
	public CommentTo selectCommentByIdComment(int idComment) {
		try {
			CommentTo commentTo = (CommentTo) jdbcTemplate.queryForObject(
					SELECT_COMMENT_BY_IDCOMMENT, new Object[] { idComment },
					new RowMapper<CommentTo>() {
						public CommentTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowComment(rs, rowNum);
						}
					});
			return commentTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUtilityDao#selectBlockByIdBlockedAndIdBlocker(int, int)
	 */
	@Override
	public BlockTo selectBlockByIdBlockedAndIdBlocker(int idBlocked, int idBlocker) {
		try {
			BlockTo blockTo = (BlockTo) jdbcTemplate.queryForObject(
					SELECT_BLOCK_BY_IDBLOCKED_AND_IDBLOCKER, new Object[] { idBlocked,idBlocker },
					new RowMapper<BlockTo>() {
						public BlockTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowBlock(rs, rowNum);
						}
					});
			return blockTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
}
