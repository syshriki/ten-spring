package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.SearchDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

@Repository
public class SearchMysqlDao implements SearchDao {
	
	private final String SELECT_USERS_BY_LIKE_USERNAME = "SELECT * FROM user AS u WHERE username LIKE ? AND u.iduser != ? AND ? NOT IN(SELECT b.iduserblocked FROM userblocks AS b WHERE b.iduserblocker=u.iduser) LIMIT ?,?";
	
	private final String SELECT_POSTS_BY_LIKE_HASHTAG = "SELECT DISTINCT up.* FROM userpost AS up LEFT JOIN userposthashtags as uph ON uph.iduserpost = up.iduserpost LEFT JOIN hashtags AS h ON uph.idhashtag = h.idhashtags LEFT JOIN user AS u ON up.iduser = u.iduser WHERE h.hash LIKE ? AND up.expiration >= NOW() AND u.isprivate = 0 AND ? NOT IN (SELECT b.iduserblocked FROM userblocks AS b WHERE b.iduserblocker=u.iduser) LIMIT ?,?";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	private TenRowMapper tenRowMapper;
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.ISearchDao#selectUsersByLikeUsername(int, java.lang.String, int, int)
	 */
	@Override
	public List<UserTo> selectUsersByLikeUsername(int idUser, String username, int offset,
			int count) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_USERS_BY_LIKE_USERNAME, new Object[] { "%"+username+"%",idUser,idUser,offset,count },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.ISearchDao#selectPostsByLikeHashtag(int, java.lang.String, int, int)
	 */
	@Override
	public List<PostTo> selectPostsByLikeHashtag(int idUser,String hashtag, int offset,
			int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(
					SELECT_POSTS_BY_LIKE_HASHTAG, new Object[] { "%"+hashtag+"%",idUser,offset,count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
}
