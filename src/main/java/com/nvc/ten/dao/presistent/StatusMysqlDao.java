package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.StatusDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.NotificationTo;

@Repository
public class StatusMysqlDao implements StatusDao {
	//Gets repost,comment mentions,likes and comment notification for posts which user has liked
    private final String SELECT_STATUS_BY_IDUSER  = "SELECT * FROM (SELECT n.* FROM unreadnotifications AS n WHERE n.id IN (SELECT DISTINCT up.iduserpost FROM userpost AS up WHERE up.iduserpost IN (SELECT iduserpost FROM userpostlikes WHERE iduser=?) AND up.expiration >= NOW() AND ? NOT IN (SELECT b.iduserblocked FROM userblocks AS b WHERE b.iduserblocker=up.iduser)) AND ? NOT IN (SELECT b.iduserblocked FROM userblocks AS b WHERE b.iduserblocker=n.affectinguser) AND (n.notificationtype=\"like\" OR n.notificationtype=\"comment\" OR n.notificationtype=\"repost\" OR n.notificationtype=\"mentioncomment\") ORDER BY n.datestamp DESC LIMIT ?,?) as notifications GROUP BY (notifications.id) ORDER BY notifications.datestamp DESC";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	private TenRowMapper tenRowMapper;

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IStatusDao#selectStatusByIdUser(int, int, int)
	 */
	@Override
	public List<NotificationTo> selectStatusByIdUser(int idUser, int offset,int count){
		try {
			List<NotificationTo> notificationTos = jdbcTemplate.query(SELECT_STATUS_BY_IDUSER,
					new Object[] { idUser,idUser,idUser,offset,count }, new RowMapper<NotificationTo>() {
						public NotificationTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowNotification(rs, rowNum);
						}
					});
			return notificationTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
}
