package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.PostDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.LikeTo;
import com.nvc.ten.models.presistent.PostHashtagTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

@Repository
public class PostMysqlDao implements PostDao {

	// General Utility TODO
	private final static String SELECT_LAST_INSERT_ID = "SELECT last_insert_id()";
	private final static String SELECT_LIKES_BY_IDPOST = "SELECT * FROM userpostlikes WHERE iduserpost = ?";
	private final static String SELECT_POSTHASHTAGS_BY_IDPOST = "SELECT * FROM userposthashtags WHERE iduserpost = ?";

	// For getting comments
	private final static String SELECT_COMMENTS_BY_IDPOST = "SELECT * FROM usercomment WHERE iduserpost = ? LIMIT ?,?";

	//For getting post
	private final static String SELECT_POST_BY_IDPOST = "SELECT * FROM userpost WHERE iduserpost = ?";
	private final static String SELECT_POST_BY_IDREPOST = "SELECT * FROM userpost WHERE iduserpostrepost = ?";
	
	//For getting post image
	private final static String SELECT_IMAGE_BY_IDPOST = "SELECT imagesrc FROM userpostimage WHERE iduserpost = ?";
	
	// For creating a post
	private final static String SELECT_IDUSER_BY_USERNAME = "SELECT COALESCE(u.iduser,0) FROM user AS u WHERE u.username = ? UNION (SELECT 0 total) LIMIT 1";
	private final static String SELECT_IDHASHTAGS = "SELECT COALESCE(h.idhashtags,0) FROM hashtags AS h WHERE h.hash = ? UNION (SELECT 0 total) LIMIT 1";
	private final static String INSERT_POST = "INSERT INTO userpost (text,iduser,iduserpostrepost,expiration,hasimage,datestamp) VALUES (?,?,?,?,?,?)";
	private final static String INSERT_IMAGE = "INSERT INTO userpostimage (iduserpost,imagesrc) VALUES (?,?)";
	private final static String INSERT_HASHTAG = "INSERT INTO hashtags (hash) VALUES (?)";
	private final static String INSERT_POSTHASHTAG = "INSERT INTO userposthashtags (iduserpost,idhashtag) VALUES (?,?)";
	private final static String CALL_INSERT_MENTION_POST_NOTIFICATION = "CALL insertMentionPostNotification(?,?,?)";

	// For deleting post
	private final static String DELETE_NOTIFICATIONS_BY_IDPOST = "DELETE FROM unreadnotifications WHERE id = ?";
	private final static String DELETE_NOTIFICATIONS_BY_IDCOMMENT = "DELETE FROM unreadnotifications WHERE idcomment = ?";
	private final static String DELETE_NOTIFICATIONS_BY_IDLIKE = "DELETE FROM unreadnotifications WHERE idlike = ?";

	private final static String DELETE_LIKES_BY_IDPOST = "DELETE FROM userpostlikes WHERE iduserpost = ?";
	private final static String DELETE_POSTHASHTAGS_BY_IDPOST = "DELETE FROM userposthashtags WHERE iduserpost = ?";
	private final static String DELETE_IMAGES_BY_IDPOST = "DELETE FROM userpostimage WHERE iduserpost = ?";
	private final static String DELETE_COMMENTS_BY_IDPOST = "DELETE FROM usercomment WHERE iduserpost = ?";
	private final static String DELETE_POST_BY_IDPOST = "DELETE FROM userpost WHERE iduserpost = ?";

	// For liking
	private final static String INSERT_LIKE = "INSERT INTO userpostlikes (iduserpost,iduser) VALUES (?,?)";
	private final static String UPDATE_POST_EXPIRATION = "UPDATE userpost SET expiration = ? WHERE iduserpost = ?";
	private final static String COUNT_LIKE_BY_IDUSER = "SELECT COUNT(iduser) FROM userpostlikes WHERE iduser = ? AND iduserpost = ?";

	// For commenting
	private final static String SELECT_COMMENT_BY_IDCOMMENT = "SELECT * FROM usercomment WHERE idusercomment = ?";
	private final static String INSERT_COMMENT = "INSERT INTO usercomment (text,iduserpost,iduser,datestamp) VALUES (?,?,?,?)";
	private final static String DELETE_COMMENT_BY_IDCOMMENT = "DELETE FROM usercomment WHERE idusercomment = ?";
	private final static String CALL_INSERT_MENTION_COMMENT_NOTIFICATION = "CALL insertMentionCommentNotification(?,?,?,?)";
	
	// For getting likers 
	private final static String SELECT_LIKERS_BY_IDPOST = "SELECT u.* FROM user AS u INNER JOIN userpostlikes AS upl ON upl.iduser=u.iduser INNER JOIN userpost AS up ON upl.iduserpost=up.iduserpost WHERE up.iduserpost=? ORDER BY u.username LIMIT ?,?";

	private final static String INSERT_NOTIFICATION = "INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,id,datestamp) VALUES (?,?,?,?,NOW())";
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	private TenRowMapper tenRowMapper;

	public int insertPost(String text, int idUser,
			int idUserPostRepost,int hasImage,Date expiration,Date datestamp) {
		int row = jdbcTemplate.update(INSERT_POST, new Object[] { text, idUser,
				idUserPostRepost, expiration,hasImage,new Date() }, new int[] { Types.VARCHAR,
				Types.INTEGER, Types.INTEGER, Types.TIMESTAMP,Types.TINYINT,Types.TIMESTAMP });
		return jdbcTemplate
				.queryForObject(SELECT_LAST_INSERT_ID, Integer.class);
	}
	
	public Integer insertNotification(String notificationType, int idAffectingUser,int idAffectedUser, int idPost) {
		int count = (Integer) jdbcTemplate.update(
				INSERT_NOTIFICATION, new Object[] { notificationType,idAffectingUser,idAffectedUser,idPost },
				new int[] { Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER });
		return count;
	}

	public int insertImage(int idUserPost, String imageSrc) {
		int row = jdbcTemplate.update(INSERT_IMAGE, new Object[] { idUserPost,
				imageSrc }, new int[] { Types.INTEGER, Types.VARCHAR });
		return jdbcTemplate
				.queryForObject(SELECT_LAST_INSERT_ID, Integer.class);
	}

	public int insertHashTag(String hash) {
		int row = jdbcTemplate.update(INSERT_HASHTAG, new Object[] { hash },
				new int[] { Types.VARCHAR });
		return jdbcTemplate
				.queryForObject(SELECT_LAST_INSERT_ID, Integer.class);
	}

	public int insertLike(int idPost, int idUser) {
		int row = jdbcTemplate.update(INSERT_LIKE, new Object[] { idPost,
				idUser }, new int[] { Types.INTEGER, Types.INTEGER });
		return jdbcTemplate
				.queryForObject(SELECT_LAST_INSERT_ID, Integer.class);
	}

	public int insertPostHashTag(int idUserPost, int idHashtags) {
		int row = jdbcTemplate.update(INSERT_POSTHASHTAG, new Object[] {
				idUserPost, idHashtags }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}

	public int callMentionPostInsert(int idMentioner, int idMentioned,
			int idUserpost) {
		int row = jdbcTemplate.update(CALL_INSERT_MENTION_POST_NOTIFICATION,
				new Object[] { idMentioner, idMentioned, idUserpost },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER });
		return row;
	}
	
	public int callMentionCommentInsert(int idMentioner, int idMentioned,
			int idUserpost, int idComment) {
		int row = jdbcTemplate.update(CALL_INSERT_MENTION_COMMENT_NOTIFICATION,
				new Object[] { idMentioner, idMentioned, idUserpost, idComment },
				new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER });
		return row;
	}

	public Integer selectIdUserByUsername(String username) {
		Integer idUser = (Integer) jdbcTemplate.queryForObject(
				SELECT_IDUSER_BY_USERNAME, new Object[] { username },
				Integer.class);
		return idUser;
	}

	public Integer selectIdHashtags(String hash) {
		Integer idhashtags = (Integer) jdbcTemplate.queryForObject(
				SELECT_IDHASHTAGS, new Object[] { hash }, Integer.class);
		return idhashtags;
	}

	public PostTo selectPostByIdPost(int idPost) {
		try {
			PostTo postTo = (PostTo) jdbcTemplate.queryForObject(

			SELECT_POST_BY_IDPOST, new Object[] { idPost },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	public List<PostTo> selectPostsByIdRepost(int idPost) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(
			SELECT_POST_BY_IDREPOST, new Object[] { idPost },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	

	
	public String selectImageByIdPost(int idPost) {
		try {
			String imageSrc = (String) jdbcTemplate.queryForObject(

			SELECT_IMAGE_BY_IDPOST, new Object[] { idPost },
					new RowMapper<String>() {
						public String mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return rs.getString("imagesrc");
						}
					});
			return imageSrc;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	public Integer countLikeByUserForPost(int idUser, int idPost) {
		Integer count = (Integer) jdbcTemplate.queryForObject(
				COUNT_LIKE_BY_IDUSER, new Object[] { idUser, idPost },
				new int[] { Types.INTEGER, Types.INTEGER }, Integer.class);
		return count;
	}

	public int updateExpiration(int idPost, Date expiration) {
		int row = jdbcTemplate.update(UPDATE_POST_EXPIRATION, new Object[] {
				expiration, idPost }, new int[] { Types.TIMESTAMP,
				Types.INTEGER });
		return row;
	}

	public int insertComment(String text, int idPost, int idUser,Date datestamp) {
		int row = jdbcTemplate.update(INSERT_COMMENT, new Object[] { text,
				idPost, idUser, datestamp }, new int[] { Types.VARCHAR, Types.INTEGER,
				Types.INTEGER, Types.TIMESTAMP });
		return jdbcTemplate
				.queryForObject(SELECT_LAST_INSERT_ID, Integer.class);
	}

	public CommentTo selectCommentByIdComment(int idComment) {
		try {
			CommentTo commentTo = (CommentTo) jdbcTemplate.queryForObject(
					SELECT_COMMENT_BY_IDCOMMENT, new Object[] { idComment },
					new RowMapper<CommentTo>() {
						public CommentTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowComment(rs, rowNum);
						}
					});
			return commentTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	public int deleteComment(int idComment) {
		return jdbcTemplate.update(DELETE_COMMENT_BY_IDCOMMENT,
				new Object[] { idComment }, new int[] { Types.INTEGER, });
	}

	public List<LikeTo> selectLikesByIdPost(int idPost) {
		try {
			List<LikeTo> likeTos = jdbcTemplate.query(SELECT_LIKES_BY_IDPOST,
					new Object[] { idPost }, new RowMapper<LikeTo>() {
						public LikeTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowLike(rs, rowNum);
						}
					});
			return likeTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	public List<CommentTo> selectCommentsByIdPost(int idPost, int offset,
			int count) {
		try {
			List<CommentTo> commentTos = jdbcTemplate.query(
					SELECT_COMMENTS_BY_IDPOST, new Object[] { idPost,offset,count },
					new RowMapper<CommentTo>() {
						public CommentTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowComment(rs, rowNum);
						}
					});
			return commentTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

//	public List<ImageTo> selectImagesByIdPost(int idPost) {
//		try {
//			List<ImageTo> imageTos = jdbcTemplate.query(
//					SELECT_IMAGES_BY_IDPOST, new Object[] { idPost },
//					new RowMapper<ImageTo>() {
//						public ImageTo mapRow(ResultSet rs, int rowNum)
//								throws SQLException {
//							return tenRowMapper.mapRowImage(rs, rowNum);
//						}
//					});
//			return imageTos;
//		} catch (EmptyResultDataAccessException ex) {
//			return null;
//		}
//	}

	public List<PostHashtagTo> selectPostHashtagsByIdPost(int idPost) {
		try {
			List<PostHashtagTo> postHashtagTo = jdbcTemplate.query(
					SELECT_POSTHASHTAGS_BY_IDPOST, new Object[] { idPost },
					new RowMapper<PostHashtagTo>() {
						public PostHashtagTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPostHashtag(rs, rowNum);
						}
					});
			return postHashtagTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	public List<UserTo> selectLikersByIdPost(int idPost, int offset, int count) {
		try {
			List<UserTo> userTo = jdbcTemplate.query(
					SELECT_LIKERS_BY_IDPOST, new Object[] { idPost,offset,count },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	public int deleteLikesByIdPost(int idPost) {
		return jdbcTemplate.update(DELETE_LIKES_BY_IDPOST,
				new Object[] { idPost }, new int[] { Types.INTEGER, });
	}

	public int deleteImagesByIdPost(int idPost) {
		return jdbcTemplate.update(DELETE_IMAGES_BY_IDPOST,
				new Object[] { idPost }, new int[] { Types.INTEGER, });
	}

	public int deleteCommentsByIdPost(int idPost) {
		return jdbcTemplate.update(DELETE_COMMENTS_BY_IDPOST,
				new Object[] { idPost }, new int[] { Types.INTEGER, });
	}

	public int deleteNotificationsByIdPost(int idPost) {
		return jdbcTemplate.update(DELETE_NOTIFICATIONS_BY_IDPOST,
				new Object[] { idPost }, new int[] { Types.INTEGER, });
	}

	public int deleteNotificationsByIdLike(int idLike) {
		return jdbcTemplate.update(DELETE_NOTIFICATIONS_BY_IDLIKE,
				new Object[] { idLike }, new int[] { Types.INTEGER, });
	}

	public int deleteNotificationsByIdComment(int idComment) {
		return jdbcTemplate.update(DELETE_NOTIFICATIONS_BY_IDCOMMENT,
				new Object[] { idComment }, new int[] { Types.INTEGER, });
	}
	
	public int deletePostHashtagsByIdPost(int idPost) {
		return jdbcTemplate.update(DELETE_POSTHASHTAGS_BY_IDPOST,
				new Object[] { idPost }, new int[] { Types.INTEGER, });
	}

	public int deletePostByIdPost(int idPost) {
		return jdbcTemplate.update(DELETE_POST_BY_IDPOST,
				new Object[] { idPost }, new int[] { Types.INTEGER, });
	}

}
