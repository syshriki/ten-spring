package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.NewsfeedDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.PostTo;

@Repository
public class NewsfeedMysqlDao implements NewsfeedDao{
	
	private final String SELECT_NEWSFEED_POSTS_BY_IDUSER = 
			"SELECT * FROM ("
				+"SELECT DISTINCT * FROM userpost WHERE ("
					+"("
						+"userpost.expiration >= NOW() AND NOT ("
							+"userpost.iduserpostrepost > 0 AND userpost.iduser = ? "
						+")"
					+") OR "
					+ "("
						+"userpost.iduserpostrepost > 0 AND ("
							+"SELECT reposted.expiration FROM userpost AS reposted WHERE reposted.iduserpost = userpost.iduserpostrepost "
						+ ") >= NOW()"
					+ ") "
				+ ") AND ("
					+ "userpost.iduser = ? OR EXISTS("
						+ " SELECT * FROM followers WHERE followers.idrequestee = ? AND followers.idrecipient = userpost.iduser AND followers.approved = 1"
					+ ") "
				+ ") ORDER BY userpost.datestamp DESC LIMIT ?,? "
			+ ") AS userpost ORDER BY userpost.datestamp DESC";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	private TenRowMapper tenRowMapper;

	public List<PostTo> selectNewsfeedPostsByIdUser(int idUser, int offset,
			int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(
					SELECT_NEWSFEED_POSTS_BY_IDUSER, new Object[] { idUser,
							idUser, idUser, offset, count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
}
