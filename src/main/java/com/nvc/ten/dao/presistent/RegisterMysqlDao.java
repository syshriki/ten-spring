package com.nvc.ten.dao.presistent;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.RegisterDao;

@Repository
public class RegisterMysqlDao implements RegisterDao {
	private final String INSERT_USER = "INSERT INTO user (username,password,description,email,imagesrc,website,isprivate) VALUES (?,?,?,?,?,?,?)";
	private final String COUNT_BY_EMAIL = "SELECT COUNT(u.iduser) FROM user AS u WHERE u.email = ?";
	private final String COUNT_BY_USERNAME = "SELECT COUNT(u.iduser) FROM user AS u WHERE u.username = ?";
	private final String SELECT_LAST_INSERT_ID = "SELECT last_insert_id()";


	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IRegisterDao#insertUser(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)
	 */
	@Override
	public int insertUser(String username,String password,String description,String email,String imageSrc,String website,int isprivate) {
		int row = jdbcTemplate.update(INSERT_USER,
				new Object[] {username,password,description,email,imageSrc,website,isprivate }, 
				new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER}
				);
		return jdbcTemplate
				.queryForObject(SELECT_LAST_INSERT_ID, Integer.class);
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IRegisterDao#countByEmail(java.lang.String)
	 */
	@Override
	public Integer countByEmail(String email) {
		try{
		Integer idUser = (Integer) jdbcTemplate.queryForObject(COUNT_BY_EMAIL,new Object[] {email},Integer.class);
		return idUser;
	} catch (EmptyResultDataAccessException ex) {
		return 0;
	}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IRegisterDao#countByUsername(java.lang.String)
	 */
	@Override
	public Integer countByUsername(String username) {
		try{
		Integer idUser = (Integer) jdbcTemplate.queryForObject(COUNT_BY_USERNAME,new Object[] {username},Integer.class);
		return idUser;
	} catch (EmptyResultDataAccessException ex) {
		return 0;
	}
	}
}
