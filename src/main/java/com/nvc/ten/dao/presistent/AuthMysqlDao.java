package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.AuthDao;
import com.nvc.ten.models.presistent.UserTo;

@Repository
public class AuthMysqlDao implements AuthDao{
	private static final String SELECT_USER_BY_USERNAME = "SELECT u.iduser,u.username,u.password FROM user AS u WHERE u.username = ?";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public UserTo selectUser(String username) {
		UserTo userTO = (UserTo) jdbcTemplate.queryForObject(SELECT_USER_BY_USERNAME,
				new Object[] { username }, new RowMapper<UserTo>() {
					public UserTo mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						UserTo u = new UserTo();
						u.setIdUser(rs.getInt("iduser"));
						u.setUsername(rs.getString("username"));
						u.setPassword(rs.getString("password"));
//						u.setDatestamp(rs.getTimestamp("datestamp"));
//						u.setDescription(rs.getString("description"));
//						u.setEmail(rs.getString("email"));
//						u.setImageSrc(rs.getString("imagesrc"));
//						u.setWebsite(rs.getString("website"));
//						u.setPrivate(rs.getInt("isprivate") == 1);
						return u;
					}
				});
		return userTO;
	}
}
