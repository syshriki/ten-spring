package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.DeviceTokenDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.DeviceTokenTo;

@Repository
public class DeviceTokenMysqlDao implements DeviceTokenDao{
	private final String UPDATE_TOKEN = "UPDATE devicetokens SET token=? WHERE iduser=? AND type = ?";
	private final String DELETE_TOKEN = "DELETE FROM devicetokens WHERE iduser=? AND type = ?";
	private final String INSERT_TOKEN = "INSERT INTO devicetokens (token,iduser,type) VALUES (?,?,?)";
	private final String SELECT_TOKEN_BY_USERNAME = "SELECT t.* FROM devicetokens t INNER JOIN user u ON u.iduser=devicetokens.iduser WHERE u.username=? AND t.type = ?";
	private final String SELECT_TOKEN_BY_IDUSER = "SELECT * FROM devicetokens WHERE iduser=? AND type = ?";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	private TenRowMapper tenRowMapper;

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.DeviceTokenDao#updateToken(java.lang.String, int)
	 */
	@Override
	public int updateToken(String token, int idUser, String type) {
		int row = jdbcTemplate.update(UPDATE_TOKEN, new Object[] { token,
				idUser, type }, new int[] { Types.VARCHAR, Types.INTEGER, Types.VARCHAR });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.DeviceTokenDao#deleteToken(int)
	 */
	@Override
	public int deleteToken(int idUser, String type) {
		return jdbcTemplate.update(DELETE_TOKEN, new Object[] { 
				idUser,type }, new int[] { Types.INTEGER, Types.VARCHAR });
	}
	
	

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.DeviceTokenDao#insertToken(java.lang.String, int)
	 */
	@Override
	public int insertToken(String token, int idUser, String type) {
		int row = jdbcTemplate.update(INSERT_TOKEN, new Object[] { token,
				idUser,type }, new int[] { Types.VARCHAR, Types.INTEGER, Types.VARCHAR });
		return row;
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.DeviceTokenDao#selectToken(java.lang.String)
	 */
	@Override
	public DeviceTokenTo selectToken(String username, String type) {
		try {
			DeviceTokenTo deviceTokenTo = (DeviceTokenTo) jdbcTemplate
					.queryForObject(SELECT_TOKEN_BY_USERNAME,
							new Object[] { username,type },
							new RowMapper<DeviceTokenTo>() {
								public DeviceTokenTo mapRow(ResultSet rs,
										int rowNum) throws SQLException {
									DeviceTokenTo deviceTokenTo = new DeviceTokenTo();
									deviceTokenTo.setIdUser(rs.getInt("iduser"));
									deviceTokenTo.setToken(rs.getString("token"));
									deviceTokenTo.setType(rs.getString("type"));
									return deviceTokenTo;
								}
							});
			return deviceTokenTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.DeviceTokenDao#selectToken(int)
	 */
	@Override
	public DeviceTokenTo selectToken(int idUser, String type) {
		try {
			DeviceTokenTo deviceTokenTo = (DeviceTokenTo) jdbcTemplate
					.queryForObject(SELECT_TOKEN_BY_IDUSER,
							new Object[] { idUser,type },
							new RowMapper<DeviceTokenTo>() {
								public DeviceTokenTo mapRow(ResultSet rs,
										int rowNum) throws SQLException {
									DeviceTokenTo deviceTokenTo = new DeviceTokenTo();
									deviceTokenTo.setIdUser(rs.getInt("iduser"));
									deviceTokenTo.setToken(rs.getString("token"));
									deviceTokenTo.setType(rs.getString("type"));
									return deviceTokenTo;
								}
							});
			return deviceTokenTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

}
