package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.UserActionDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.BlockTo;
import com.nvc.ten.models.presistent.PasswordResetToken;
import com.nvc.ten.models.presistent.UserTo;

@Repository
public class UserActionMysqlDao implements UserActionDao {
	private final static String UPDATE_FOLLOWER_APPROVE = "UPDATE followers SET approved=1 WHERE idrequestee=? AND idrecipient=?";
	private final static String UPDATE_FOLLOWER_REJECT = "UPDATE followers SET approved=-1 WHERE idrequestee=? AND idrecipient=?";
	private final static String UPDATE_FOLLOWER_PENDING = "UPDATE followers SET approved=0 WHERE idrequestee=? AND idrecipient=?";
	

	private final static String INSERT_BLOCK = "INSERT INTO userblocks (iduserblocked,iduserblocker) VALUES (?,?)";
	private final static String DELETE_FOLLOW = "DELETE FROM followers WHERE idrequestee = ? AND idrecipient = ?";
	private final static String DELETE_BLOCK = "DELETE FROM userblocks WHERE iduserblocked = ? AND iduserblocker = ?";
	private final static String DELETE_FOLLOWER_PENDING = "DELETE FROM followers WHERE followers.idrequestee = ? AND followers.idrecipient = ? AND followers.approved = 0";
    private final static String SELECT_BLOCK_BY_IDBLOCKED_AND_IDBLOCKER = "SELECT * FROM userblocks WHERE iduserblocked = ? AND iduserblocker = ? LIMIT 1";
	private final static String INSERT_FOLLOWER_APPROVED = "INSERT INTO followers (idrequestee,idrecipient,approved) VALUES (?,?,1)";
	private final static String INSERT_FOLLOWER_PENDING = "INSERT INTO followers (idrequestee,idrecipient,approved) VALUES (?,?,0)";
	private final static String SELECT_FOLLOWERS_PENDING = "SELECT * FROM user WHERE EXISTS (SELECT * FROM followers WHERE user.iduser = followers.idrequestee AND followers.idrecipient = ? AND followers.approved = 0) ORDER BY user.username LIMIT ?,?";
	
	private final static String INSERT_PASSWORDRESETTOKEN = "INSERT INTO passwordresettoken (token,iduser,expirydate) VALUES (?,?,?)";
	private final static String SELECT_PASSWORDRESETTOKEN_BY_TOKEN = "SELECT * FROM passwordresettoken WHERE token = ? AND used = 0";
	private final static String UPDATE_PASSWORDRESETTOKEN_BY_TOKEN = "UPDATE passwordresettoken SET used=1 WHERE token = ?";
	
	
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	TenRowMapper tenRowMapper;//TODO make all column names consistent with TO names so I can get rid of this
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#updateFollowerApprove(int, int)
	 */
	@Override
	public int updateFollowerApprove(int idRequestee,int idRecipient) {
		int row = jdbcTemplate.update(UPDATE_FOLLOWER_APPROVE, new Object[] {
				idRequestee, idRecipient }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#updateFollowerReject(int, int)
	 */
	@Override
	public int updateFollowerReject(int idRequestee,int idRecipient) {
		int row = jdbcTemplate.update(UPDATE_FOLLOWER_REJECT, new Object[] {
				idRequestee, idRecipient }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#updateFollowerPending(int, int)
	 */
	@Override
	public int updateFollowerPending(int idRequestee,int idRecipient) {
		int row = jdbcTemplate.update(UPDATE_FOLLOWER_PENDING, new Object[] {
				idRequestee, idRecipient }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#insertBlock(int, int)
	 */
	@Override
	public int insertBlock(int idBlocked,int idBlocker) {
		int row = jdbcTemplate.update(INSERT_BLOCK, new Object[] {
				idBlocked, idBlocker }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#deleteBlock(int, int)
	 */
	@Override
	public int deleteBlock(int idBlocked,int idBlocker) {
		int row = jdbcTemplate.update(DELETE_BLOCK, new Object[] {
				idBlocked, idBlocker }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#deleteFollow(int, int)
	 */
	@Override
	public int deleteFollow(int idRequestee,int idRecipient) {
		int row = jdbcTemplate.update(DELETE_FOLLOW, new Object[] {
				idRequestee, idRecipient }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#selectBlockByIdBlockedAndIdBlocker(int, int)
	 */
	@Override
	public BlockTo selectBlockByIdBlockedAndIdBlocker(int idBlocked,int idBlocker) {
		try {
			BlockTo blockTo = (BlockTo) jdbcTemplate.queryForObject(
					SELECT_BLOCK_BY_IDBLOCKED_AND_IDBLOCKER, new Object[] { idBlocked,idBlocker },
					new RowMapper<BlockTo>() {
						public BlockTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowBlock(rs, rowNum);
						}
					});
			return blockTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#selectFollowerPending(int, int, int)
	 */
	@Override
	public List<UserTo> selectFollowerPending(int idUser, int offset, int count) {
		try {
			List<UserTo> userTo = jdbcTemplate.query(
					SELECT_FOLLOWERS_PENDING, new Object[] { idUser,offset,count },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#insertFollowApproved(int, int)
	 */
	@Override
	public int insertFollowApproved(int idRequestee,int idRecipient) {
		int row = jdbcTemplate.update(INSERT_FOLLOWER_APPROVED, new Object[] {
				idRequestee, idRecipient }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#insertFollowPending(int, int)
	 */
	@Override
	public int insertFollowPending(int idRequestee,int idRecipient) {
		int row = jdbcTemplate.update(INSERT_FOLLOWER_PENDING, new Object[] {
				idRequestee, idRecipient }, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#deleteFollowPending(int, int)
	 */
	@Override
	public int deleteFollowPending(int idRequestee,int idRecipient) {
		int row = jdbcTemplate.update(DELETE_FOLLOWER_PENDING, new Object[] {
				idRequestee,idRecipient}, new int[] { Types.INTEGER,
				Types.INTEGER });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#insertPasswordResetToken(java.lang.String, int, java.util.Date)
	 */
	@Override
	public int insertPasswordResetToken(String token,int idUser, Date expiry) {
		int row = jdbcTemplate.update(INSERT_PASSWORDRESETTOKEN, new Object[] {
				token, idUser,expiry}, new int[] { Types.VARCHAR,
				Types.INTEGER,Types.TIMESTAMP });
		return row;
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#selectPasswordResetTokenByToken(java.lang.String)
	 */
	@Override
	public PasswordResetToken selectPasswordResetTokenByToken(String token) {
		try {
			PasswordResetToken passwordResetToken = (PasswordResetToken) jdbcTemplate.queryForObject(
					SELECT_PASSWORDRESETTOKEN_BY_TOKEN, new Object[] { token },
					new RowMapper<PasswordResetToken>() {
						public PasswordResetToken mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPasswordResetToken(rs, rowNum);
						}
					});
			return passwordResetToken;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nvc.ten.dao.IUserActionDao#updatePasswordresetByToken(java.lang.String)
	 */
	@Override
	public int updatePasswordresetByToken(String token) {
		int row = jdbcTemplate.update(UPDATE_PASSWORDRESETTOKEN_BY_TOKEN, new Object[] {
				token }, new int[] { Types.VARCHAR});
		return row;
	}
}
