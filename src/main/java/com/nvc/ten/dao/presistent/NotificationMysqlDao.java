package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.NotificationDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.NotificationTo;

@Repository
public class NotificationMysqlDao implements NotificationDao{
	private final static String SELECT_UNREADNOTIFICATIONS_BY_IDUSER = "SELECT n.* FROM unreadnotifications AS n WHERE affecteduser = ? AND isread = 0 ORDER BY n.datestamp DESC LIMIT ?,?";
	private final static String SELECT_UNREADNOTIFICATION_BY_IDNOTIFICATION = "SELECT n.* FROM unreadnotifications AS n WHERE idunreadnotifications=?";

	private final static String COUNT_UNREADNOTIFICATIONS_BY_IDUSER = "SELECT COUNT(*) FROM unreadnotifications AS n WHERE affecteduser = ? AND isread = 0";
	private final static String UPDATE_UNREADNOTIFICATION_READ = "UPDATE unreadnotifications SET isread = 1  WHERE idunreadnotifications = ?";
	private final static String INSERT_NOTIFICATION = "INSERT INTO unreadnotifications(notificationtype,id,affectinguser,affecteduser,datestamp) VALUES (?,?,?,?,NOW())";
	private final static String INSERT_LIKE_NOTIFICATION = "INSERT INTO unreadnotifications(notificationtype,id,affectinguser,affecteduser,idlike,datestamp) VALUES (?,?,?,?,?,NOW())";
	private final static String INSERT_COMMENT_NOTIFICATION = "INSERT INTO unreadnotifications(notificationtype,id,affectinguser,affecteduser,idcomment,datestamp) VALUES (?,?,?,?,?,NOW())";
	private final static String INSERT_NOTIFICATION_NO_IDPOST = "INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,datestamp) VALUES (?,?,?,NOW())";
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	private TenRowMapper tenRowMapper;


	public NotificationTo selectNotificationByIdNotification(int idNotification) {
		try {
			NotificationTo commentTo = (NotificationTo) jdbcTemplate.queryForObject(
					SELECT_UNREADNOTIFICATION_BY_IDNOTIFICATION, new Object[] { idNotification },
					new RowMapper<NotificationTo>() {
						public NotificationTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowNotification(rs, rowNum);
						}
					});
			return commentTo;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	public Integer insertNotification(String notificationType, int idPost, int idAffectingUser,int idAffectedUser) {
		int count = (Integer) jdbcTemplate.update(
				INSERT_NOTIFICATION, new Object[] { notificationType,idPost,idAffectingUser,idAffectedUser },
				new int[] { Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER });
		return count;
	}
	public Integer insertCommentNotification(String notificationType, int idPost, int idAffectingUser,int idAffectedUser, int idComment) {
		int count = (Integer) jdbcTemplate.update(
				INSERT_COMMENT_NOTIFICATION, new Object[] { notificationType,idPost,idAffectingUser,idAffectedUser,idComment },
				new int[] { Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER });
		return count;
	}
	public Integer insertLikeNotification(String notificationType, int idPost, int idAffectingUser,int idAffectedUser, int idLike) {
		int count = (Integer) jdbcTemplate.update(
				INSERT_LIKE_NOTIFICATION, new Object[] { notificationType,idPost,idAffectingUser,idAffectedUser,idLike },
				new int[] { Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER,Types.INTEGER });
		return count;
	}
	public Integer insertNotification(String notificationType, int idAffectingUser,int idAffectedUser) {
		int count = (Integer) jdbcTemplate.update(
				INSERT_NOTIFICATION_NO_IDPOST, new Object[] { notificationType,idAffectingUser,idAffectedUser },
				new int[] { Types.VARCHAR,Types.INTEGER,Types.INTEGER });
		return count;
	}
	
	public Integer countUnreadNotificationByIdUser(int idUser) {
		Integer count = (Integer) jdbcTemplate.queryForObject(
				COUNT_UNREADNOTIFICATIONS_BY_IDUSER, new Object[] { idUser },
				new int[] { Types.INTEGER }, Integer.class);
		return count;
	}
	
	public Integer updateUnreadNotificationRead(int idNotification) {
		int count = (Integer) jdbcTemplate.update(
				UPDATE_UNREADNOTIFICATION_READ, new Object[] { idNotification },
				new int[] { Types.INTEGER });
		return count;
	}
	
	public List<NotificationTo> selectUnreadNotificationByIdUser(int idUser, int offset, int count){
		try {
			List<NotificationTo> notificationTos = jdbcTemplate.query(SELECT_UNREADNOTIFICATIONS_BY_IDUSER,
					new Object[] { idUser,offset,count }, new RowMapper<NotificationTo>() {
						public NotificationTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowNotification(rs, rowNum);
						}
					});
			return notificationTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
}
