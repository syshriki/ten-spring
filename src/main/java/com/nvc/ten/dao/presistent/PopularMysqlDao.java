package com.nvc.ten.dao.presistent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nvc.ten.dao.PopularDao;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

@Repository
public class PopularMysqlDao implements PopularDao{
	private final String SELECT_TOPTIMED_LOCAL_BY_IDUSER = "SELECT userpost.*,COUNT(distinct iduserpostlikes) as timeweight FROM userpost LEFT JOIN userpostlikes as likecount ON likecount.iduserpost = userpost.iduserpost INNER JOIN userpostimage ON userpost.iduserpost = userpost.iduserpost WHERE userpost.expiration >= NOW() AND (userpost.iduser IN ( SELECT idrecipient from followers WHERE idrequestee = ?  AND followers.approved = 1) OR (userpost.iduser = ?) ) AND (userpost.iduserpostrepost = 0) GROUP BY userpost.iduserpost ORDER BY timeweight DESC LIMIT ?,?";

	private final String SELECT_TOPTIMERS_LOCAL_BY_IDUSER = "SELECT user.* FROM(SELECT userpostlikes.*,COUNT(*) as likes FROM userpostlikes WHERE userpostlikes.iduser <> ? AND userpostlikes.iduser IN (SELECT followers.idrecipient FROM followers WHERE followers.idrequestee = ? AND followers.approved = 1) GROUP BY userpostlikes.iduser ORDER BY likes DESC LIMIT ?,?) toplikers LEFT JOIN user ON user.iduser = toplikers.iduser";

	private final String SELECT_TOPTIMED_GLOBAL_BY_IDUSER  = "SELECT userpost.*,COUNT(distinct iduserpostlikes) as timeweight FROM userpost LEFT JOIN user as u ON u.iduser = userpost.iduser LEFT JOIN userpostimage ON userpost.iduserpost = userpostimage.iduserpost LEFT JOIN userpostlikes as likecount ON likecount.iduserpost = userpost.iduserpost WHERE userpost.iduser IN ( SELECT iduser from user WHERE user.isprivate = 0 AND userpost.expiration >= NOW()) AND userpost.iduserpostrepost = 0 GROUP BY userpost.iduserpost ORDER BY timeweight DESC LIMIT ?,?";

	private final String SELECT_TOPTIMERS_GLOBAL_BY_IDUSER  = "Select user.* FROM (SELECT userpostlikes.iduser,COUNT(*) as likes FROM userpostlikes GROUP BY userpostlikes.iduser ORDER BY likes DESC LIMIT ?,?) toplikers LEFT JOIN user ON user.iduser = toplikers.iduser";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	private TenRowMapper tenRowMapper;
	
	public List<UserTo> selectTopTimerLocalByIdPost(int idUser, int offset, int count) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_TOPTIMERS_LOCAL_BY_IDUSER, new Object[] { idUser,idUser,offset,count },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	public List<UserTo> selectTopTimerGlobalByIdPost(int idUser, int offset, int count) {
		try {
			List<UserTo> userTos = jdbcTemplate.query(
					SELECT_TOPTIMERS_GLOBAL_BY_IDUSER, new Object[] { offset,count },
					new RowMapper<UserTo>() {
						public UserTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowUser(rs, rowNum);
						}
					});
			return userTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	public List<PostTo> selectTopTimedLocalByIdPost(int idUser, int offset,
			int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(
					SELECT_TOPTIMED_LOCAL_BY_IDUSER, new Object[] {	idUser, idUser, offset, count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	public List<PostTo> selectTopTimedGlobalByIdPost(int idUser, int offset,
			int count) {
		try {
			List<PostTo> postTos = jdbcTemplate.query(
					SELECT_TOPTIMED_GLOBAL_BY_IDUSER, new Object[] { offset, count },
					new RowMapper<PostTo>() {
						public PostTo mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return tenRowMapper.mapRowPost(rs, rowNum);
						}
					});
			return postTos;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

}
