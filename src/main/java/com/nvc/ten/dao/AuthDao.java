package com.nvc.ten.dao;

import com.nvc.ten.models.presistent.UserTo;

public interface AuthDao {
	public UserTo selectUser(String username);
}
