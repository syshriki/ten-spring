package com.nvc.ten.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nvc.ten.dao.UtilityDao;
import com.nvc.ten.models.json.User;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.models.json.UserExtended.Friended;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.FollowTo;
import com.nvc.ten.models.presistent.ImageTo;
import com.nvc.ten.models.presistent.LikeTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;

@Service
public class ServiceUtility {
	@Autowired
	UtilityDao utilityDao;
	
	//To find out if a user is following another user
	public boolean isFollowing(int idRecipient, int idRequestee){
		if(idRecipient==idRequestee)//This condition should not really be in here, not sure where it should go
			return true;
		FollowTo followTo = utilityDao.selectFollower(idRequestee, idRecipient);
		return followTo!=null && followTo.getApproved()==1;
	}
	
	public boolean isPending(int idRecipient, int idRequestee){
		FollowTo followTo = utilityDao.selectFollower(idRequestee, idRecipient);
		return followTo!=null && followTo.getApproved()==0;
	}
	
	public FollowTo getFollowTo(int idRecipient, int idRequestee){
		return utilityDao.selectFollower(idRequestee, idRecipient);
	}
	
	//Find out if the users post is private
	public boolean isPrivate(PostTo postTo){
		UserTo userTo = utilityDao.selectUserByIdUser(postTo.getIdUser());
		return userTo.isPrivate();
	}
	
	//TODO Should find better way to do this than grabbing post again
	public boolean isExpired(PostTo postTo){
		if(postTo.getIdRepost()!=0)
			postTo = getPostTo(postTo.getIdRepost());
		return System.currentTimeMillis()> postTo.getExpiration().getTime();
	}
	
	public UserTo getUserToByIdUser(int idUser){
		return utilityDao.selectUserByIdUser(idUser);
	}
	
	public UserTo getUserToByEmail(String email){
		return utilityDao.selectUserByEmail(email);
	}
	
	public UserTo getUserToByUsername(String username){
		return utilityDao.selectUserByUsername(username);
	}
	
	public String getPostImageSrc(int idPost){
		ImageTo imageTo = utilityDao.selectImageByIdPost(idPost);
		return imageTo == null ? null : imageTo.getImageSrc();
	}
	
	public PostTo getPostTo(int idPost){
		return utilityDao.selectPostByIdPost(idPost);
	}
	
	public boolean isLikedByUser(int idPost, int idUser){
		return utilityDao.selectLikeByIdPostAndIdUser(idPost,idUser)!=null;
	}
	
	public boolean isCommentedOnByUser(int idPost, int idUser){
		return utilityDao.selectCommentByIdPostAndIdUser(idPost,idUser)!=null;
	}

	public Friended getFriended(int idRequestee,int idRecipient){
		if(idRequestee!=idRecipient){
			FollowTo followTo = utilityDao.selectFollower(idRequestee, idRecipient);
			return followTo!= null ? UserExtended.Friended.GetValue(followTo.getApproved()) : UserExtended.Friended.NONE;
		}else{
			return UserExtended.Friended.NONE;
		}
	}
	
	public boolean isBlocked(int idBlocked, int idBlocker){
		if(idBlocked!=idBlocker)
			return utilityDao.selectBlockByIdBlockedAndIdBlocker(idBlocked,idBlocker) != null;
		else
			return false;
	}
	
	public boolean isReposted(int idPost, int idUser){
		return utilityDao.selectPostByRepostedAndIdUser(idPost, idUser) !=null;
	}
	
	public CommentTo getCommentTo(int idComment){
		return utilityDao.selectCommentByIdComment(idComment);
	}
	
	public LikeTo getLikeTo(int idLike){
		return utilityDao.selectLikeByIdLike(idLike);
	}
}
