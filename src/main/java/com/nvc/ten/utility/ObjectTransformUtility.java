package com.nvc.ten.utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.nvc.ten.models.json.Comment;
import com.nvc.ten.models.json.Notification;
import com.nvc.ten.models.json.Notification.NotificationType;
import com.nvc.ten.models.json.Post;
import com.nvc.ten.models.json.User;
import com.nvc.ten.models.json.UserExtended;
import com.nvc.ten.models.presistent.CommentTo;
import com.nvc.ten.models.presistent.LikeTo;
import com.nvc.ten.models.presistent.NotificationTo;
import com.nvc.ten.models.presistent.PostTo;
import com.nvc.ten.models.presistent.UserTo;
//TODO lookup Spring Converter 
public class ObjectTransformUtility {
	@Autowired
	private ServiceUtility serviceUtility;
	
	@Autowired
	private ImageUtility imageUtility;
	
	public List<UserExtended> userTosToUserExtended(List<UserTo> userTos){
		return userTosToUserExtended(userTos,0);
	}
	
	public List<UserExtended> userTosToUserExtended(List<UserTo> userTos,int idUser){
		List<UserExtended> users = new ArrayList<>();
		userTos.forEach( (userTo) -> users.add(userToToUserExtended(userTo,idUser)) );
		return users;
	}
	
	public UserExtended userToToUserExtended(UserTo userTo){
		return userToToUserExtended(userTo,0);
	}
	
	public UserExtended userToToUserExtended(UserTo userTo,int idUser){
		UserExtended userExtended = new UserExtended();
		userExtended.setDescription(userTo.getDescription());
		userExtended.setEmail(userTo.getEmail());
		userExtended.setIdUser(userTo.getIdUser());
		//userExtended.setImageSrc(imageUtility.getBase64StringImage(userTo.getImageSrc(),userTo.getIdUser()));
		userExtended.setUsername(userTo.getUsername());
		userExtended.setIsPrivate(userTo.isPrivate());
		userExtended.setWebsite(userTo.getWebsite());
		if(idUser == userTo.getIdUser())
			userExtended.setFriended(UserExtended.Friended.SELF);
		else if(idUser!=0){
			userExtended.setFriended(serviceUtility.getFriended(idUser, userTo.getIdUser()));
			userExtended.setBlocked(serviceUtility.isBlocked(userTo.getIdUser(), idUser));
		}
		return userExtended;
	}
	
	public User userToToUser(UserTo userTo){
		User user = new User();
		user.setIdUser(userTo.getIdUser());
		//user.setImageSrc(imageUtility.getBase64StringImage(userTo.getImageSrc(),userTo.getIdUser()));
		user.setUsername(userTo.getUsername());
		//Issue #27
		user.setIsPrivate(userTo.isPrivate());
		return user;
	}
	
	public List<User> userTosToUsers(List<UserTo> userTos){
		List<User> users = new ArrayList<>();
		userTos.forEach( (userTo) -> users.add(userToToUser(userTo)) );
		return users;
	}
	
	public Comment commentToToComment(CommentTo commentTo){
		Comment comment = new Comment();
		comment.setDatestamp(commentTo.getDatestamp());	
		comment.setIdComment(commentTo.getIdComment());
		comment.setText(commentTo.getText());
		comment.setUser(this.userToToUser(serviceUtility.getUserToByIdUser(commentTo.getIdUser())));
		return comment;
	}
	
	public List<Comment> commentToToComment(List<CommentTo> commentTos){
		List<Comment> comments = new ArrayList<Comment>();
		commentTos.forEach((commentTo)->comments.add(commentToToComment(commentTo)));
		return comments;
	}
	
	public List<Post> postTosToPosts(List<PostTo> postTos,int idUser){
		List<Post> posts = new ArrayList<>();
		postTos.forEach((postTo)->posts.add(postToToPost(postTo,idUser)));
		return posts;
	}
	
	public Post postToToPost(PostTo postTo,int idUser){
		if(postTo==null)
			return null;
		
		boolean reposted = postTo.getIdRepost() > 0;
		
		//Save User and idPost incase repost happens
		User user = null;
		int idPost = postTo.getIdPost();
		Post post = new Post();
		
		if(reposted){
			//Save datestamp
			
			Date datestamp = postTo.getDatestamp();
			//Save original user
			user = this.userToToUser(serviceUtility.getUserToByIdUser(postTo.getIdUser())); 
			
			//Set post to original post
			postTo = serviceUtility.getPostTo(postTo.getIdRepost()); 
			
			//Issue #72
			if(postTo==null)
				return null;
			
			post.setOriginalDatestamp(postTo.getDatestamp());
			
			//Apply old date to post
			postTo.setDatestamp(datestamp);
			
		}
		
		
		post.setDatestamp(postTo.getDatestamp());
		post.setExpiration(postTo.getExpiration());
		post.setIdPost(idPost);
		post.setText(postTo.getText());
		post.setUserPoster(this.userToToUser(serviceUtility.getUserToByIdUser(postTo.getIdUser())));
		post.setHasImage(postTo.getHasImage()==1);
		//post.setImage(imageUtility.getBase64StringImage(serviceUtility.getPostImageSrc(postTo.getIdPost()),postTo.getIdUser()));
		
		if(reposted)
			post.setUserReposter(user);
		
		post.setIsReposted(serviceUtility.isReposted(postTo.getIdPost(), idUser));
		post.setLiked(serviceUtility.isLikedByUser(postTo.getIdPost(), idUser));
		post.setCommented(serviceUtility.isCommentedOnByUser(postTo.getIdPost(), idUser));
		return post;
	}
	
	public List<Notification> notificationTosToNotifications(List<NotificationTo> notificationTos,int idUser){
		List<Notification> notifications = new ArrayList<>();
		for(NotificationTo notificationTo : notificationTos){
			Notification notification = notificationToToNotification(notificationTo,idUser);
			//Issue #71
			if(NotificationType.FRIENDREQUEST == notification.getType()|| 
					NotificationType.FOLLOWING == notification.getType()|| 
					NotificationType.FOLLOW == notification.getType() || notification.getPost()!=null)
				notifications.add(notification);
		}
		return notifications;
	}
	
	public Notification notificationToToNotification(NotificationTo notificationTo){
		return notificationToToNotification(notificationTo,0);
	}
	public Notification notificationToToNotification(NotificationTo notificationTo, int idUser){
		Notification notification = new Notification();
		notification.setUser(this.userToToUser(serviceUtility.getUserToByIdUser(notificationTo.getIdAffecting())));
		notification.setDatestamp(notificationTo.getDatestamp());
		notification.setIdNotification(notificationTo.getIdNotification());
		switch(Notification.NotificationType.valueOf(notificationTo.getNotificationType().toUpperCase())){
		case COMMENT:
			CommentTo commentTo = serviceUtility.getCommentTo(notificationTo.getIdComment());
			if(commentTo!=null){
				PostTo postTo = serviceUtility.getPostTo(commentTo.getIdPost());
				if(postTo!=null)
					notification.setPost(this.postToToPost(postTo,idUser));
				//Issue #26
				notification.setMessage(commentTo.getText());
			}
			notification.setType(Notification.NotificationType.COMMENT);
			break;
		case LIKE:
			LikeTo likeTo = serviceUtility.getLikeTo(notificationTo.getIdLike());
			if(likeTo!=null){
				PostTo postTo = serviceUtility.getPostTo(likeTo.getIdPost());
				if(postTo!=null)
					notification.setPost(this.postToToPost(postTo, idUser));
				//Issue #26
//				notification.setMessage(notification.getUser().getUsername() +" liked your post");
				notification.setMessage(postTo.getText());

			}
			notification.setType(Notification.NotificationType.LIKE);
			break;
		case FRIENDREQUEST:
			notification.setMessage(notification.getUser().getUsername()+" wants to follow you");
			notification.setType(Notification.NotificationType.FRIENDREQUEST);
			break;
		case FOLLOW:
			notification.setMessage("You are now following "+notification.getUser().getUsername());
			notification.setType(Notification.NotificationType.FOLLOW);
			break;
		case FOLLOWING:
			//Issue #91 message change
			notification.setMessage(notification.getUser().getUsername()+" accepted your friend request");
			notification.setType(Notification.NotificationType.FOLLOWING);
			break;
		case MENTIONPOST:
			notification.setPost(this.postToToPost(serviceUtility.getPostTo(notificationTo.getId()),idUser));
			notification.setMessage(notification.getUser().getUsername()+" mentioned you in a post");
			notification.setType(Notification.NotificationType.MENTIONPOST);
			break;
		case MENTIONCOMMENT:
			CommentTo mentioned = serviceUtility.getCommentTo(notificationTo.getIdComment());
			if(mentioned!=null){
				PostTo postTo = serviceUtility.getPostTo(mentioned.getIdPost());
				if(postTo!=null)
					notification.setPost(this.postToToPost(postTo,idUser));
				notification.setMessage(mentioned.getText());
			}
			notification.setType(Notification.NotificationType.MENTIONCOMMENT);
			break;
		case POSTEXPIRED:
			PostTo postTo = serviceUtility.getPostTo(notificationTo.getId());
			if(postTo!=null)
				notification.setPost(this.postToToPost(postTo,idUser));
			notification.setMessage("Post has expired");
			notification.setType(Notification.NotificationType.POSTEXPIRED);
			break;
		case REPOST:
			PostTo repost = serviceUtility.getPostTo(notificationTo.getId());
			if(repost!=null)
				notification.setPost(this.postToToPost(repost,idUser));
			//Issue #26
//			notification.setMessage("Your post has been reposted");
			notification.setMessage(repost.getText());
			notification.setType(Notification.NotificationType.REPOST);
			break;
		default:
			System.out.println("Unknown notification type "+notificationTo.getNotificationType());
		}
		return notification;
	}
	
}
