package com.nvc.ten.utility;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

public class ImageUtility {
	@Value("${upload.dir}")
	private String uploadDir;

	@Value("${post.image.height.max}")
	private int MAX_IMAGE_HEIGHT;
	
	@Value("${post.image.width.max}")
	private int MAX_IMAGE_WIDTH;

	public static final Map<String, String> SUPPORTED_IMAGE_FORMATS = Collections
			.unmodifiableMap(new HashMap<String, String>() {
				{
					put("image/png", "png");
					put("image/jpg", "jpg");
					put("image/jpeg", "jpeg");
					put("image/gif", "gif");
				}
			});
	
	public boolean isSupportedImageFormat(MultipartFile imageFile) {
		return this.SUPPORTED_IMAGE_FORMATS.containsKey(imageFile
				.getContentType());
	}
	
	public String getBase64StringImage(String path,int idUser){
		return path==null || path.equals("") ? "" : Base64.getEncoder().encodeToString(this.loadImage(idUser + "/" + path));
	}
	
	public String getImageType(String extension){
		String result = null;
		for(Entry<String,String> imageFormat : this.SUPPORTED_IMAGE_FORMATS.entrySet()){
			if(extension.equals(imageFormat.getValue())){
				result = imageFormat.getKey();
				break;
			}
		}
		return result;
	}
	
	public void saveImageFileToDisk(MultipartFile multipartImage, String filename,int idUser){
		BufferedImage image = null;
		try (InputStream in = new ByteArrayInputStream(multipartImage.getBytes())){
			image = ImageIO.read(in);
			saveImageFileToDisk(image, filename,idUser);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public BufferedImage scaleImage(MultipartFile image) {
		BufferedImage originalImage = null;
		try {
			InputStream in = new ByteArrayInputStream(image.getBytes());
			originalImage = ImageIO.read(in);
			
			// Get image dimensions
			int height = originalImage.getHeight();
			int width = originalImage.getWidth();

			if (height > MAX_IMAGE_HEIGHT || width > MAX_IMAGE_WIDTH) {
				double widthScaleFactor = width / MAX_IMAGE_WIDTH;
				double heightScaleFactor = height / MAX_IMAGE_HEIGHT;
				double scaleFactor = Math.max(widthScaleFactor,
						heightScaleFactor);
				if (scaleFactor > 1) {// image needs to be scaled
					int scaledWidth = (int) Math.round(width / scaleFactor);
					int scaledHeight = (int) Math.round(height / scaleFactor);

					// Resize
					return Scalr.resize(originalImage, scaledWidth,
							scaledHeight);
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return originalImage;
	}
	
	public void saveImageFileToDisk(BufferedImage imageFile, String filename,int idUser) {
		if (imageFile != null) {
			try {
				File file = new File(uploadDir + "/" + idUser);
				if (!file.exists())
					file.mkdirs();
				file = new File(uploadDir + "/" + idUser + "/" + filename);
				if (!file.exists())
					file.createNewFile();
				ImageIO.write(imageFile, filename.substring(
						filename.lastIndexOf(".") + 1, filename.length()), file);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public byte [] loadImage(String path){
		try {
			BufferedImage bufferedImage = ImageIO.read(new File(uploadDir+"/"+path));
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			try {
				ImageIO.write(bufferedImage, path.substring(path.lastIndexOf(".")+1, path.length()), outputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			byte[] imgBytes = outputStream.toByteArray();
			return imgBytes;
		} catch (IOException e) {
			return null;
		}
	}
}
