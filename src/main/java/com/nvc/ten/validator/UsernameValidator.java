package com.nvc.ten.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<Username, String>{

	private boolean required;
	@Override
	public void initialize(Username constraintAnnotation) {
		this.required = constraintAnnotation.required();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		//Username vallidation
		//[a-zA-Z0-9._] allowed character
		//6-20 characters long
		//no _ or . at the beginning
		//no __ or _. or ._ or .. inside
		//no _ or . at the end
		String regex = "^(?=.{6,20}$)(?![_])(?!.*[_]{2})[a-zA-Z0-9_]+$";
		if(required){
			if(value==null)
				return false;
			return value.matches(regex);
		}else{
			return value != null && !value.isEmpty() ? value.matches(regex) : true;
		}
	}

}
