package com.nvc.ten.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String>{
	public static final String PASSWORD_VALIDATION_REGEX = "^(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])\\S{6,}$";
	private boolean required;
	@Override
	public void initialize(Password constraintAnnotation) {
		this.required = constraintAnnotation.required();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
//		Password validation
//		(?=.*[a-z])       # a lower case letter must occur at least once
//		(?=.*[A-Z])       # an upper case letter must occur at least once
//		(?=.*[@#$%^&+=])  # a special character must occur at least once
//		(?=\S+$)          # no whitespace allowed in the entire string
//		.{6,}             # anything, at least six places though
		
		if(required){
			if(value==null)
				return false;
			boolean valid = value.matches(PASSWORD_VALIDATION_REGEX);
			return valid;
		}else{
			return value != null && !value.isEmpty() ? value.matches(PASSWORD_VALIDATION_REGEX) : true;
		}
	}

}
