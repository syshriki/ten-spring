package com.nvc.ten.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.nvc.ten.service.AuthenticationService;

@Configuration
@ComponentScan("com.nvc")
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	Environment env;

	@Autowired
	AuthenticationService authenticationService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		/*
		 * "if you are only creating a service that is used by non-browser
		 * clients, you will likely want to disable CSRF protection."
		 */
		http
			.csrf().disable()
			.authorizeRequests()
				.antMatchers("/","/register","/action/forgotPassword","/action/resetPassword").permitAll()
				.anyRequest().authenticated()
				.and()
			.httpBasic(); //Basic authentication enabled
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.userDetailsService(authenticationService).passwordEncoder(
				passwordEncoder());
	}
}
