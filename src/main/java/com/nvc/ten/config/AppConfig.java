package com.nvc.ten.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.exceptions.InvalidSSLConfig;
import com.nvc.ten.dao.helper.TenRowMapper;
import com.nvc.ten.push.INotifier;
import com.nvc.ten.push.Notifier;
import com.nvc.ten.push.NotifierIosImpl;
import com.nvc.ten.utility.ImageUtility;
import com.nvc.ten.utility.ObjectTransformUtility;


@Configuration
@ComponentScan("com.nvc.ten")
@EnableWebMvc
@Import({ SecurityConfig.class })
public class AppConfig {
	@Autowired
	Environment env;
	
	@Bean
	public ObjectTransformUtility objectTransformUtility(){
		return new ObjectTransformUtility();
	}
	
	//TODO beanPropertyRowMapper
	@Bean
	public TenRowMapper tenRowMapper(){
		return new TenRowMapper();
	}
	
	@Bean
	public ImageUtility imageUtility(){
		return new ImageUtility();
	}
	
	@Bean INotifier notifierIosImpl(){
		return new Notifier();
	}
	
	@Bean
	@Profile("prod")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
		dataSource.setUrl(env.getRequiredProperty("jdbc.url"));
		dataSource.setUsername(env.getRequiredProperty("jdbc.username"));
		dataSource.setPassword(env.getRequiredProperty("jdbc.password"));
		return dataSource;
	}
	
	@Bean
	@Profile("test")
	public DataSource dataSource() {
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder
			.setType(EmbeddedDatabaseType.HSQL) //.H2 or .DERBY
			.addScript("db/scripts/Dump20160627-1.sql")
			.build();
		return db;
	}
	
	@Bean
    public JavaMailSender javaMailSender() {
        final JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();
        mailSenderImpl.setHost(env.getProperty("smtp.host"));
        mailSenderImpl.setPort(env.getProperty("smtp.port", Integer.class));
        mailSenderImpl.setProtocol(env.getProperty("smtp.protocol"));
      //  mailSenderImpl.setUsername(env.getProperty("smtp.username"));
      //  mailSenderImpl.setPassword(env.getProperty("smtp.password"));
        final Properties javaMailProps = new Properties();
        javaMailProps.put("mail.smtp.auth", false);
        //javaMailProps.put("mail.smtp.starttls.enable", true);
        //javaMailProps.put("mail.smtp.from", env.getProperty("email.from"));
        //javaMailProps.put("mail.debug", "true");
        mailSenderImpl.setJavaMailProperties(javaMailProps);
        return mailSenderImpl;
    }

	@Bean
	public ApnsService apnsService() throws IOException {
	       ClassPathResource classPathResource = new ClassPathResource(env.getProperty("apns.cert.path"));
	        InputStream inputStream = classPathResource.getInputStream();
			return APNS.newService()
					.withCert(inputStream, env.getProperty("apns.cert.password"))
					//.withProductionDestination()
					.withSandboxDestination()
					.build();
	}
/*     @Bean
     public MessageSource messageSource() {
     final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
     messageSource.setBasename("classpath:messages");
     messageSource.setUseCodeAsDefaultMessage(true);
     messageSource.setDefaultEncoding("UTF-8");
     messageSource.setCacheSeconds(0);
		return messageSource;
     }*/

/*	@Bean
	public DataSource getDataSourceTest() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
		dataSource.setUrl(env.getRequiredProperty("jdbc.url.test"));
		dataSource.setUsername(env.getRequiredProperty("jdbc.username"));
		dataSource.setPassword(env.getRequiredProperty("jdbc.password"));
		return dataSource;
	}*/
}