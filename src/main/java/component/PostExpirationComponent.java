package component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

import com.nvc.ten.models.PostExpirationPoint;

public class PostExpirationComponent {
	private static PriorityBlockingQueue<PostExpirationPoint> postExpirationPoints  = new PriorityBlockingQueue<>(); 
	private static List<PostExpirationPoint> postExpirationPointsToIncrement  = new ArrayList<>(); 
	
	public static void addPostExpirationPoint(int idUser,  int idPost, Date expiration) {
		postExpirationPoints.add(new PostExpirationPoint(expiration,idUser,idPost));
	}
	
	//If someone liked the post increment the timer on the post
	public static void incrementPostExpirationPoint(int idUser,  int idPost, Date expiration){
		postExpirationPointsToIncrement.add(new PostExpirationPoint(expiration,idUser,idPost));
	}
	
	//Send notification to user
	public static int notifyUser(int idUser,  int idPost){
		//TODO
		return 0;
	}
	
	//After a point expires it is removed
	public static PostExpirationPoint remove(){
		if(postExpirationPoints.peek() != null &&
				postExpirationPoints.peek().getExpiration().before(new Date())){
			return postExpirationPoints.poll();
		}
		return null;
	}
	
	public static void increment(){
		for(PostExpirationPoint postExpirationPoint : postExpirationPointsToIncrement){
			if(postExpirationPoints.contains(postExpirationPoint)){
				//Remove and re-add point with new expiration date
				postExpirationPoints.remove(postExpirationPoint);
				postExpirationPoints.add(postExpirationPoint);
			}
		}
	}
	
	//Main method responsible for monitoring 
	public static void main(String[] args) {
		while(true){
			//Check if post is expired, if so remove it and notify user
			PostExpirationPoint removed = remove();
			if(removed!=null);
				//TODO notify user

			increment();
			
		}
	}

}

